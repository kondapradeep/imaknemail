﻿using Nop.Core.Domain.Messages;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nop.Web.Models.Messages;
using FluentValidation.Results;
using FluentValidation;

namespace Nop.Web.Validators.Messages
{
    public partial class EmailAccountValidator : BaseNopValidator<EmailAccount>
    {
        public EmailAccountValidator(ILocalizationService localizationService,
            EmailAccountSettings emailAccountSettings)
        {
            if (emailAccountSettings.EmailEnabled && emailAccountSettings.EmailRequired)
            {
                RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.EmailAccounts.Fields.Email.Required"));
            }
            if (emailAccountSettings.DisplayNameEnabled && emailAccountSettings.DisplayNameRequired)
            {
                RuleFor(x => x.DisplayName).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.EmailAccounts.Fields.DisplayName.Required"));
            }
            if (emailAccountSettings.HostEnabled && emailAccountSettings.HostRequired)
            {
                RuleFor(x => x.Host).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.EmailAccounts.Fields.Host.Required"));
            }
            if (emailAccountSettings.PortEnabled && emailAccountSettings.PortRequired)
            {
                RuleFor(x => x.Port).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.EmailAccounts.Fields.Port.Required"));
            }
            if (emailAccountSettings.UsernameEnabled && emailAccountSettings.UsernameRequired)
            {
                RuleFor(x => x.Username).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.EmailAccounts.Fields.Username.Required"));
            }
            if (emailAccountSettings.PasswordEnabled && emailAccountSettings.PasswordRequired)
            {
                RuleFor(x => x.Password).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.EmailAccounts.Fields.Password.Required"));
            }
        }
    }
}