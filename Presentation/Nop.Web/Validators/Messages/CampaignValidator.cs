﻿using Nop.Core.Domain.Messages;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using FluentValidation.Results;

namespace Nop.Web.Validators.Messages
{
    public partial class CampaignValidator : BaseNopValidator<CampaignModel>
    {
        public CampaignValidator (ILocalizationService localizationService, CampaignSettings campaignSettings)
        {
            if (campaignSettings.NameRequired && campaignSettings.NameEnabled)
            {
                RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Promotions.Campaigns.Fields.Name.Required"));
            }
            if (campaignSettings.SubjectRequired && campaignSettings.SubjectEnabled)
            {
                RuleFor(x => x.Subject).NotEmpty().WithMessage(localizationService.GetResource("Admin.Promotions.Campaigns.Fields.Subject.Required"));
            }
            if (campaignSettings.BodyRequired && campaignSettings.BodyEnabled)
            {
                RuleFor(x => x.Body).NotEmpty().WithMessage(localizationService.GetResource("Admin.Promotions.Campaigns.Fields.Body.Required"));
            }
        }
    } 
}