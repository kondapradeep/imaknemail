update LocaleStringResource set ResourceValue = 'Member Info' where ResourceName='Account.CustomerInfo'
update LocaleStringResource set ResourceValue = 'New Member' where ResourceName='Account.Login.NewCustomer'
update LocaleStringResource set ResourceValue = 'Returning Member' where ResourceName='Account.Login.ReturningCustomer'
update LocaleStringResource set ResourceValue = 'Invalid User Id or Password' where ResourceName='Account.Login.WrongCredentials.CustomerNotExist'
update LocaleStringResource set ResourceValue = 'Added a new member (ID = {0})' where ResourceName= 'ActivityLog.AddNewCustomer'
update LocaleStringResource set ResourceValue = 'Members' where ResourceName= 'Admin.Customers'
update LocaleStringResource set ResourceValue = 'Members' where ResourceName= 'Admin.Customers.Customers'
update LocaleStringResource set ResourceValue = 'Member Roles' where ResourceName= 'Admin.Customers.Customers.CustomerRoles'
update LocaleStringResource set ResourceValue = 'Online Members' where ResourceName= 'Admin.Customers.OnlineCustomers' 
update LocaleStringResource set ResourceValue = 'Member Info' where ResourceName= 'Admin.Customers.OnlineCustomers.Fields.CustomerInfo'
update LocaleStringResource set ResourceValue = 'Member Reports' where ResourceName= 'Admin.Customers.Reports'
update LocaleStringResource set ResourceValue = 'Member Roles' where ResourceName='Admin.Customers.CustomerRoles'
update LocaleStringResource set Resourcevalue='Statistics' where ResourceName='admin.dashboard.storestatistics'
update LocaleStringResource set Resourcevalue='Email Marketing News' where ResourceName='Admin.NopCommerceNews'





insert into LocaleStringResource values (1,'Account.CustomerCampaigns.Edit','EditCampaigns')
insert into LocaleStringResource values (1,'Account.CustomerCampaigns','Campaigns')
insert into LocaleStringResource values (1,'Admin.Configuration.Settings.CustomerUser.CampaignFormFields','Campaigns form fields')
insert into LocaleStringResource values (1,'Admin.Configuration.Settings.CustomerUser.CampaignFormFields.Description',
		'You can create and manage the campaign form fields available after registration below')


insert into LocaleStringResource values (1,'Admin.Customers.Customers.Campaigns.Added',
'The new campaign has been added sucessfully')

insert into LocaleStringResource values (1,'Admin.Customers.Customers.Campaigns.Updated',
'The new campaign has been updated sucessfully')

insert into LocaleStringResource values (1,'Account.CustomerCampaigns.AddNew','Add new campaign')

insert into Setting values('campaignsettings.nameenabled','True',0)
insert into Setting values('campaignsettings.namerequired','False',0)

insert into Setting values('campaignsettings subjectenabled','True',0)
insert into Setting values('campaignsettings.subjectrequired','False',0)

insert into Setting values('campaignsettings.bodyenabled','True',0)		
insert into Setting values('campaignsettings.bodyrequired','False',0)

insert into LocaleStringResource values (1,'Account.EmailAccounts','EmailAccounts')
insert into LocaleStringResource values (1,'Account.Campaigns','Campaigns')

insert into LocaleStringResource values (1,'Account.Dashboard','Dashboard')
insert into LocaleStringResource values (1,'admin.configuration.settings.customeruser.emailaccountformfields','Email Account form fields')
insert into LocaleStringResource values (1,'admin.configuration.settings.customeruser.emailaccountformfields.description',
		'You can create and manage the emailaccount form fields available after registration below')


insert into LocaleStringResource values (1,'Account.CustomerEmailAccounts','Email Accounts')
insert into LocaleStringResource values (1,'account.customeremailaccounts.addnew','Add new email account')
insert into LocaleStringResource values (1,'account.customeremailaccounts.edit','Edit email account')
insert into LocaleStringResource values (1,'Account.CustomerEmailAccounts.NoEmailAccounts','No EmailAccounts')
insert into LocaleStringResource values (1,'Account.CustomerCampaigns.NoCampaigns','No campaigns')

insert into LocaleStringResource values(1,'Admin.Configuration.EmailAccounts.Fields.Email.Required','Email Address is required')
insert into LocaleStringResource values(1,'Admin.Configuration.EmailAccounts.Fields.DisplayName.Required','Email display name is required')
insert into LocaleStringResource values(1,'Admin.Configuration.EmailAccounts.Fields.Host.Required','Host is required')
insert into LocaleStringResource values(1,'Admin.Configuration.EmailAccounts.Fields.Port.Required','Port no is required')
insert into LocaleStringResource values(1,'Admin.Configuration.EmailAccounts.Fields.Username.Required','Username is required')
insert into LocaleStringResource values(1,'Admin.Configuration.EmailAccounts.Fields.Password.Required','Password is required')



 --new tables permission provider in admin
insert into PermissionRecord values('Admin area. Manage Email Groups','ManageEmailGroups','Email Management')
insert into PermissionRecord values('Admin area. Manage Email Lists','ManageEmailLists','Email Management')

insert into PermissionRecord_Role_Mapping values(52,1)
insert into PermissionRecord_Role_Mapping values(52,3)
insert into PermissionRecord_Role_Mapping values(53,1)
insert into PermissionRecord_Role_Mapping values(53,3)

--newly add strings of the Email management


insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailList.ID','ID')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailList.EmailListTemplate','EmailList Template')

insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailList.EmailGroup','Email Group')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailList.Email','Email')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailLists','Email List')

insert into LocaleStringResource values(1,'Admin.EmailManagement','Email Management')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroup.Name','Group name')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroup.EmailGroupTemplate','EmailGroup Template')

insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups','Email Group')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.AddNew','Add A New Email Group')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.BackToList','back to email group list')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.EditEmailGroupDetails','Edit Email Group Details')

insert into LocaleStringResource values(1,'Admin.EmailMangement.EmailGroups.Info','EmailGroup Info')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.Manage','Manage EmailGroups')

insert into LocaleStringResource values(1,'Admin.EmailManagement.EditEmailListDetails','Edit EmailList Details')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailList.BackToList','back to email list')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailList.AddNew','Add A New Email List')


insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailLists.Manage','Manage Email Lists')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.EmailGroups','Email Groups')

insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.Added','The new email group has been added successfully.')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.Updated','The email group has been updated successfully.')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.Deleted','The email group has been deleted successfully.')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailLists.Added','The new email list has been added successfully.')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailLists.Updated','The email list has been updated successfully.')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailLists.Deleted','The email list has been deleted successfully.')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailLists.Imported','Email list(s) have been imported successfully')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailLists.ImportFromExcelTip','Imported email lists are distinguished by email.If the email added already exists, then its corresponding email list will be updated')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.NoEmailGroupsAvailable','No emailgroups available. Create at least one emailgroup before mapping.')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.SaveBeforeEdit','You need to save the email list before you can map email groups for this email list page.')
insert into LocaleStringResource values(1,'ActivityLog.AddNewEmailList','Added a new email list')
insert into LocaleStringResource values(1,'ActivityLog.EditEmailList','Edited a email list')
insert into LocaleStringResource values(1,'ActivityLog.DeleteEmailList','Deleted a email list')
insert into LocaleStringResource values(1,'ActivityLog.AddNewEmailGroup','Added a new email group')
insert into LocaleStringResource values(1,'ActivityLog.EditEmailGroup','Edited a email group')
insert into LocaleStringResource values(1,'ActivityLog.DeleteEmailGroup','Deleted a email group')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.SearchEmailGroup','Email Group')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroups.AddNewEmailGroup','Add new email group')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroup.AddNewEmailList','Add new email list')




insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailGroup.GroupName.Required','Group name is required')
insert into LocaleStringResource values(1,'Admin.EmailManagement.EmailList.Email.Required','Email is required')
insert into LocaleStringResource values(1,'Admin.Plans','Plans')
insert into LocaleStringResource values(1,'Admin.Plans.Manage','Manage Plans')
insert into LocaleStringResource values(1,'Admin.Plans.BulkEdit','Bulk edit plans')

update LocaleStringResource set Resourcevalue='Plan Name' where ResourceName='Admin.Catalog.Products.List.SearchProductName'
update LocaleStringResource set Resourcevalue='Manage Plans' where ResourceName='Admin.Catalog.Products.Manage'
update LocaleStringResource set Resourcevalue='Bulk edit plans' where ResourceName='Admin.Catalog.BulkEdit'
update LocaleStringResource set Resourcevalue='Plan Type' where ResourceName='Admin.Catalog.Products.List.SearchProductType'
update LocaleStringResource set Resourcevalue='Plan Name' where ResourceName='Admin.Catalog.BulkEdit.List.SearchProductName'




update LocaleStringResource set Resourcevalue='Plan Info' where ResourceName='Admin.Catalog.Products.Info'
update LocaleStringResource set Resourcevalue='Plan Attributes' where ResourceName='Admin.Catalog.Products.ProductAttributes'
update LocaleStringResource set Resourcevalue='Add a New Plan' where ResourceName='Admin.Catalog.Products.AddNew'
update LocaleStringResource set Resourcevalue='back to plan list' where ResourceName='Admin.Catalog.Products.BackToList'



--20-07-2016
update LocaleStringResource set Resourcevalue='System Queue' where ResourceName='Admin.System'
update LocaleStringResource set Resourcevalue='Your subscription plan has been sucessfully processed!' where ResourceName='Checkout.YourOrderHasBeenSuccessfullyProcessed'
update LocaleStringResource set Resourcevalue='Subscription number' where ResourceName='Checkout.OrderNumber'
update LocaleStringResource set Resourcevalue='Click here for subscription details' where ResourceName='Checkout.PlacedOrderDetails'
update LocaleStringResource set Resourcevalue='Subscription information' where ResourceName='Order.OrderInformation'
update LocaleStringResource set Resourcevalue='Date' where ResourceName='Order.OrderDate'
update LocaleStringResource set Resourcevalue='Status' where ResourceName='Order.OrderStatus'
update LocaleStringResource set Resourcevalue='Total' where ResourceName='Order.OrderTotal'
update LocaleStringResource set Resourcevalue='Payment Method' where ResourceName='Order.PaymentMethod'
update LocaleStringResource set Resourcevalue='Plan' where ResourceName='Order.Product(s)'
update LocaleStringResource set Resourcevalue='Subscription Total' where ResourceName='Order.TotalDiscount'
update LocaleStringResource set Resourcevalue='Re-Subscription' where ResourceName='Order.Reorder'
update LocaleStringResource set Resourcevalue='Confirm Subscription' where ResourceName='Checkout.ConfirmOrder'
update LocaleStringResource set Resourcevalue='Subscription Details' where ResourceName='PageTitle.OrderDetails'


insert into LocaleStringResource values(1,'Order.PaymentMethod','Payment Method')
--21-07-2016
update LocaleStringResource set Resourcevalue='Plan name' where ResourceName='Admin.Catalog.Products.Fields.Name'
update LocaleStringResource set Resourcevalue='Plan type' where ResourceName='Admin.Catalog.Products.Fields.ProductType'
update LocaleStringResource set Resourcevalue='Edit Plan Details' where ResourceName='Admin.Catalog.Products.EditProductDetails'
update LocaleStringResource set Resourcevalue='Here you can see a list of Subscriptions in which this plan was purchased.' where ResourceName='Admin.Catalog.Products.PurchasedWithOrders.Hint'
insert into LocaleStringResource values (1,'AlWsoulEmailMarketing','AL WSOUL EMAIL MARKETING')

--24-08-2016
update LocaleStringResource set Resourcevalue='Member Roles' where ResourceName='Admin.Customers.Customers.List.CustomerRoles'
update LocaleStringResource set Resourcevalue='Member Roles' where ResourceName='Admin.Customers.Customers.List.CustomerRoles'
update LocaleStringResource set Resourcevalue='The new member  role has been added successfully.' where ResourceName='admin.customers.customerroles.added'
update LocaleStringResource set Resourcevalue='Add a new member role' where ResourceName='admin.customers.customerroles.addnew'
update LocaleStringResource set Resourcevalue='back to member role list' where ResourceName='admin.customers.customerroles.backtolist'
update LocaleStringResource set Resourcevalue='Add a new member value' where ResourceName='admin.customers.customerattributes.values.addnew'
update LocaleStringResource set Resourcevalue='Member Info' where ResourceName='admin.customers.customers.info'
update LocaleStringResource set Resourcevalue='Filter by member role.' where ResourceName='admin.customers.customers.list.customerroles.hint'

update LocaleStringResource set Resourcevalue='The members date of birth.' where ResourceName='admin.customers.customers.fields.dateofbirth.hint'
update LocaleStringResource set Resourcevalue='The members email.' where ResourceName='admin.customers.customers.fields.email.hint'
update LocaleStringResource set Resourcevalue='The members first name.' where ResourceName='admin.customers.customers.fields.firstname.hint'

update LocaleStringResource set Resourcevalue='The members gender.' where ResourceName='admin.customers.customers.fields.gender.hint'
update LocaleStringResource set Resourcevalue='Determines whether the member is tax exempt.' where ResourceName='admin.customers.customers.fields.istaxexempt.hint'
update LocaleStringResource set Resourcevalue='The members last name.' where ResourceName='admin.customers.customers.fields.lastname.hint'
update LocaleStringResource set Resourcevalue='The members password.' where ResourceName='admin.customers.customers.fields.password.hint'
update LocaleStringResource set Resourcevalue='The system name of the member role.' where ResourceName='admin.customers.customers.fields.systemname.hint'
update LocaleStringResource set Resourcevalue='The members username.'where ResourceName='admin.customers.customers.fields.username.hint'



update LocaleStringResource set Resourcevalue='Choose a vendor associated to this member account.
 When associated this member will be able to login to the chosen vendor portal and manage his plans and orders.'
 where ResourceName='admin.customers.customers.fields.vendor.hint'

update LocaleStringResource set Resourcevalue='Custom member attributes' where ResourceName='admin.customers.customerattributes'
update LocaleStringResource set Resourcevalue='Add a new member attribute' where ResourceName='admin.customers.customerattributes.addnew'
update LocaleStringResource set Resourcevalue='back to member attribute list' where ResourceName='admin.customers.customerattributes.backtolist'
update LocaleStringResource set Resourcevalue='Edit member attribute details' where ResourceName='admin.customers.customerattributes.editattributedetails'
update LocaleStringResource set Resourcevalue='If the default form fields are not enough for your needs, then you can manage additional member attributes below.' 
where ResourceName='admin.customers.customerattributes.description'

update LocaleStringResource set Resourcevalue='When an attribute is required, the member must choose an appropriate attribute value before they can continue.'
 where ResourceName='admin.customers.customerattributes.fields.isrequired.hint'

update LocaleStringResource set Resourcevalue='The name of the member attribute.' where ResourceName='admin.customers.customerattributes.fields.name.hint'

update LocaleStringResource set Resourcevalue='Add a new member value' where ResourceName='admin.customers.customerattributes.values.addnew'

update LocaleStringResource set Resourcevalue='Edit member value details' where ResourceName='admin.customers.customerattributes.values.editvaluedetails'
update LocaleStringResource set Resourcevalue='Determines whether this attribute value is pre selected for the member.' where ResourceName='admin.customers.customerattributes.values.fields.ispreselected.hint'
update LocaleStringResource set Resourcevalue='The name of the member value.' where ResourceName='admin.customers.customerattributes.values.fields.name.hint'
update LocaleStringResource set Resourcevalue='You need to save the member attribute before you can add values for this member attribute page.' 
where ResourceName='admin.customers.customerattributes.values.savebeforeedit'























