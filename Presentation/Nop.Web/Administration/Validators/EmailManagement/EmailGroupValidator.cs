﻿using FluentValidation;
using Nop.Admin.Models.EmailManagement;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Admin.Validators.EmailManagement
{
    public class EmailGroupValidator : BaseNopValidator<EmailGroupModel>
    {
        public EmailGroupValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.EmailManagement.EmailGroup.GroupName.Required"));
        }
    }
}