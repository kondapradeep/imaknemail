﻿using FluentValidation;
using Nop.Admin.Models.EmailManagement;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Admin.Validators.EmailManagement
{
    public class EmailListValidator : BaseNopValidator<EmailListModel>
    {
        public EmailListValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Admin.EmailManagement.EmailList.Email.Required"));
        }
    }
}