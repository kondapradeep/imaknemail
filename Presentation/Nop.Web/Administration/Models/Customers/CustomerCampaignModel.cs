﻿using Nop.Admin.Models.Messages;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Customers
{
    public class CustomerCampaignModel : BaseNopModel
    {
       public int CustomerId { get; set; }

       public CampaignModel Campaign { get; set; }
    }
}