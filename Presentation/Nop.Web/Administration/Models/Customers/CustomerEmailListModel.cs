﻿using Nop.Admin.Models.EmailManagement;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Customers
{
    public partial class CustomerEmailListModel : BaseNopEntityModel
    {

        public int CustomerId { get; set; }

        public EmailListModel EmailList { get; set; }
    }
}