﻿using Nop.Admin.Models.Messages;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Customers
{
    public class CustomerEmailAccountModel : BaseNopEntityModel
    {
        public int CustomerId { get; set; }

        public EmailAccountModel EmailAccount { get; set;}
    }
}