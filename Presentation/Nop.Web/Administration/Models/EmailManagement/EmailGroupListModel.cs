﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Web.Framework;

namespace Nop.Admin.Models.EmailManagement
{
    public partial class EmailGroupListModel : BaseNopModel
    {
        [AllowHtml]
        public string SearchEmailGroupName { get; set; }
    }
}