﻿using Nop.Admin.Models.Customers;
using Nop.Admin.Models.Stores;
using Nop.Admin.Validators.EmailManagement;
using Nop.Web.Framework;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using FluentValidation.Attributes;

namespace Nop.Admin.Models.EmailManagement
{
    [Validator(typeof(EmailListValidator))]
    public partial class EmailListModel : BaseNopEntityModel, ILocalizedModel<EmailListLocalizedModel>
    {
         public EmailListModel()
        {
            Locales = new List<EmailListLocalizedModel>();
            AvailableEmailListTemplates = new List<SelectListItem>();
            AvailableEmailGroups = new List<SelectListItem>();
            //AvailableCountries = new List<SelectListItem>();
            //AvailableStates = new List<SelectListItem>();
         }


         [NopResourceDisplayName("Admin.EmailManagement.EmailList.ID")]
         public override int Id { get; set; }

         [NopResourceDisplayName("Admin.EmailManagement.EmailList.EmailListTemplate")]
         public int EmailListTemplateId { get; set; }
         public IList<SelectListItem> AvailableEmailListTemplates { get; set; }

        

         [NopResourceDisplayName("Admin.Address.Fields.FirstName")]
         [AllowHtml]
         public string FirstName { get; set; }

         [NopResourceDisplayName("Admin.Address.Fields.LastName")]
         [AllowHtml]
         public string LastName { get; set; }
         [NopResourceDisplayName("Admin.EmailManagement.EmailList.Email")]
         [AllowHtml]
         public string Email { get; set; }

         //[NopResourceDisplayName("Admin.Address.Fields.Country")]
         //public int? CountryId { get; set; }

         [NopResourceDisplayName("Admin.Address.Fields.Country")]
         [AllowHtml]
         public string Country { get; set; }

         //[NopResourceDisplayName("Admin.Address.Fields.StateProvince")]
         //public int? StateProvinceId { get; set; }

         [NopResourceDisplayName("Admin.Address.Fields.StateProvince")]
         [AllowHtml]
         public string State { get; set; }

         [NopResourceDisplayName("Admin.Address.Fields.City")]
         [AllowHtml]
         public string City { get; set; }

         [NopResourceDisplayName("Admin.Address.Fields.Address1")]
         [AllowHtml]
         public string Address1 { get; set; }

         [NopResourceDisplayName("Admin.Address.Fields.Address2")]
         [AllowHtml]
         public string Address2 { get; set; }

         [NopResourceDisplayName("Admin.Address.Fields.ZipPostalCode")]
         [AllowHtml]
         public string ZipPostalCode { get; set; }

         [NopResourceDisplayName("Admin.Address.Fields.PhoneNumber")]
         [AllowHtml]
         public string PhoneNumber { get; set; }

         //public IList<SelectListItem> AvailableCountries { get; set; }
         //public IList<SelectListItem> AvailableStates { get; set; }

         [NopResourceDisplayName("Admin.Catalog.Products.Fields.MetaKeywords")]
         [AllowHtml]
         public string MetaKeywords { get; set; }

         [NopResourceDisplayName("Admin.Catalog.Products.Fields.MetaDescription")]
         [AllowHtml]
         public string MetaDescription { get; set; }

         [NopResourceDisplayName("Admin.Catalog.Products.Fields.MetaTitle")]
         [AllowHtml]
         public string MetaTitle { get; set; }

         [NopResourceDisplayName("Admin.Catalog.Products.Fields.SeName")]
         [AllowHtml]
         public string SeName { get; set; }

         [NopResourceDisplayName("Admin.Catalog.Products.Fields.DisplayOrder")]
         public int DisplayOrder { get; set; }

         [NopResourceDisplayName("Admin.Catalog.Products.Fields.Published")]
         public bool Published { get; set; }

         [NopResourceDisplayName("Admin.Catalog.Products.Fields.CreatedOn")]
         public DateTime? CreatedOn { get; set; }
         [NopResourceDisplayName("Admin.Catalog.Products.Fields.UpdatedOn")]
         public DateTime? UpdatedOn { get; set; }

         public IList<EmailListLocalizedModel> Locales { get; set; }

         //ACL (customer roles)
         [NopResourceDisplayName("Admin.Catalog.Products.Fields.SubjectToAcl")]
         public bool SubjectToAcl { get; set; }
         [NopResourceDisplayName("Admin.Catalog.Products.Fields.AclCustomerRoles")]
         public List<CustomerRoleModel> AvailableCustomerRoles { get; set; }
         public int[] SelectedCustomerRoleIds { get; set; }

         //Store mapping
         [NopResourceDisplayName("Admin.Catalog.Products.Fields.LimitedToStores")]
         public bool LimitedToStores { get; set; }
         [NopResourceDisplayName("Admin.Catalog.Products.Fields.AvailableStores")]
         public List<StoreModel> AvailableStores { get; set; }
         public int[] SelectedStoreIds { get; set; }

         //email groups
         public IList<SelectListItem> AvailableEmailGroups { get; set; }
         public partial class EmailGroupEmailListModel : BaseNopEntityModel
         {
             [NopResourceDisplayName("Admin.EmailManagement.EmailList.EmailGroup")]
             public string EmailGroup { get; set; }

             public int EmailListId { get; set; }

             public int EmailGroupId { get; set; }

             [NopResourceDisplayName("Admin.Catalog.Products.Manufacturers.Fields.DisplayOrder")]
             public int DisplayOrder { get; set; }
         }

    }
    public partial class EmailListLocalizedModel : ILocalizedModelLocal
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.EmailManagement.EmailList.Email")]
        [AllowHtml]
        public string Email { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.Fields.MetaKeywords")]
        [AllowHtml]
        public string MetaKeywords { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.Fields.MetaDescription")]
        [AllowHtml]
        public string MetaDescription { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.Fields.MetaTitle")]
        [AllowHtml]
        public string MetaTitle { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.Fields.SeName")]
        [AllowHtml]
        public string SeName { get; set; }
    }
}