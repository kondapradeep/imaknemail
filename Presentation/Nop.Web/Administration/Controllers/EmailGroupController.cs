﻿using Nop.Admin.Models.EmailManagement;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.EmailManagement;
using Nop.Services.Customers;
using Nop.Services.EmailManagement;
using Nop.Services.ExportImport;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Nop.Core;

namespace Nop.Admin.Controllers
{
    public partial class EmailGroupController : BaseAdminController
    {
        #region Fields

        private readonly IEmailManagementService _emailManagementService;
        private readonly IEmailManagementTemplateService _emailGroupTemplateService;
        private readonly IEmailManagementListService _emailListService;
        private readonly ICustomerService _customerService;
        private readonly IStoreService _storeService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IExportManager _exportManager;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IAclService _aclService;
        private readonly IPermissionService _permissionService;
        private readonly CatalogSettings _catalogSettings;
        private readonly IWorkContext _workContext;

        #endregion
        
        #region Constructors

        public EmailGroupController(IEmailManagementService emailManagementService,
            IEmailManagementTemplateService emailGroupTemplateService,
            IEmailManagementListService emailListService,
            ICustomerService customerService, 
            IStoreService storeService,
            IStoreMappingService storeMappingService,
            IUrlRecordService urlRecordService,
            ILanguageService languageService, 
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService, 
            IExportManager exportManager,
            ICustomerActivityService customerActivityService, 
            IAclService aclService,
            IPermissionService permissionService,
            CatalogSettings catalogSettings,
            IWorkContext workContext)
        {
            this._emailManagementService = emailManagementService;
            this._emailGroupTemplateService = emailGroupTemplateService;
            this._customerService = customerService;
            this._storeService = storeService;
            this._storeMappingService = storeMappingService;
            this._urlRecordService = urlRecordService;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._localizedEntityService = localizedEntityService;
            this._exportManager = exportManager;
            this._customerActivityService = customerActivityService;
            this._aclService = aclService;
            this._permissionService = permissionService;
            this._catalogSettings = catalogSettings;
            this._emailListService = emailListService;
            this._workContext = workContext;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void UpdateLocales(EmailGroup emailGroup, EmailGroupModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(emailGroup,
                                                               x => x.Name,
                                                               localized.Name,
                                                               localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(emailGroup,
                                                           x => x.Description,
                                                           localized.Description,
                                                           localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(emailGroup,
                                                           x => x.MetaKeywords,
                                                           localized.MetaKeywords,
                                                           localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(emailGroup,
                                                           x => x.MetaDescription,
                                                           localized.MetaDescription,
                                                           localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(emailGroup,
                                                           x => x.MetaTitle,
                                                           localized.MetaTitle,
                                                           localized.LanguageId);

                //search engine name
                var seName = emailGroup.ValidateSeName(localized.SeName, localized.Name, false);
                _urlRecordService.SaveSlug(emailGroup, seName, localized.LanguageId);
            }
        }

        [NonAction]
        protected virtual void PrepareTemplatesModel(EmailGroupModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            var templates = _emailGroupTemplateService.GetAllEmailGroupTemplates();
            foreach (var template in templates)
            {
                model.AvailableEmailGroupTemplates.Add(new SelectListItem
                {
                    Text = template.Name,
                    Value = template.Id.ToString()
                });
            }
        }

        [NonAction]
        protected virtual void PrepareAclModel(EmailGroupModel model, EmailGroup emailGroup, bool excludeProperties)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            model.AvailableCustomerRoles = _customerService
                .GetAllCustomerRoles(true)
                .Select(cr => cr.ToModel())
                .ToList();
            if (!excludeProperties)
            {
                if (emailGroup != null)
                {
                    model.SelectedCustomerRoleIds = _aclService.GetCustomerRoleIdsWithAccess(emailGroup);
                }
            }
        }

        [NonAction]
        protected virtual void SaveEmailGroupAcl(EmailGroup emailGroup, EmailGroupModel model)
        {
            var existingAclRecords = _aclService.GetAclRecords(emailGroup);
            var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
            foreach (var customerRole in allCustomerRoles)
            {
                if (model.SelectedCustomerRoleIds != null && model.SelectedCustomerRoleIds.Contains(customerRole.Id))
                {
                    //new role
                    if (existingAclRecords.Count(acl => acl.CustomerRoleId == customerRole.Id) == 0)
                        _aclService.InsertAclRecord(emailGroup, customerRole.Id);
                }
                else
                {
                    //remove role
                    var aclRecordToDelete = existingAclRecords.FirstOrDefault(acl => acl.CustomerRoleId == customerRole.Id);
                    if (aclRecordToDelete != null)
                        _aclService.DeleteAclRecord(aclRecordToDelete);
                }
            }
        }

        [NonAction]
        protected virtual void PrepareStoresMappingModel(EmailGroupModel model, EmailGroup emailGroup, bool excludeProperties)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            model.AvailableStores = _storeService
                .GetAllStores()
                .Select(s => s.ToModel())
                .ToList();
            if (!excludeProperties)
            {
                if (emailGroup != null)
                {
                    model.SelectedStoreIds = _storeMappingService.GetStoresIdsWithAccess(emailGroup);
                }
            }
        }

        [NonAction]
        protected virtual void SaveStoreMappings(EmailGroup emailGroup, EmailGroupModel model)
        {
            var existingStoreMappings = _storeMappingService.GetStoreMappings(emailGroup);
            var allStores = _storeService.GetAllStores();
            foreach (var store in allStores)
            {
                if (model.SelectedStoreIds != null && model.SelectedStoreIds.Contains(store.Id))
                {
                    //new store
                    if (existingStoreMappings.Count(sm => sm.StoreId == store.Id) == 0)
                        _storeMappingService.InsertStoreMapping(emailGroup, store.Id);
                }
                else
                {
                    //remove store
                    var storeMappingToDelete = existingStoreMappings.FirstOrDefault(sm => sm.StoreId == store.Id);
                    if (storeMappingToDelete != null)
                        _storeMappingService.DeleteStoreMapping(storeMappingToDelete);
                }
            }
        }

        #endregion

        #region List

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

           // var model = new EmailGroupListModel();

            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            //var emailGroups = _emailManagementService.GetAllEmailGroups(model.SearchEmailGroupName,
            //    command.Page - 1, command.PageSize, true);
            
            //var gridModel = new DataSourceResult
            //{
            //    Data = emailGroups.Select(x => x.ToModel()),
            //    Total = emailGroups.TotalCount
            //};

            //return Json(gridModel);
            //var emailGroups = _emailManagementService.GetAllEmailGroups();
            var emailGroups = _emailManagementService.GetAllEmailGroupsByCustomerId(_workContext.CurrentCustomer.Id);
            var gridModel = new DataSourceResult
            {
                Data = emailGroups.Select(x =>
                {
                    var model = x.ToModel();
                    
                    return model;
                }),
                Total = emailGroups.Count
            };
            return Json(gridModel);

        }

        #endregion

        #region Create / Edit / Delete

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            var model = new EmailGroupModel();
            //locales
            AddLocales(_languageService, model.Locales);
            //templates
            PrepareTemplatesModel(model);
            //ACL
            PrepareAclModel(model, null, false);
            //Stores
            PrepareStoresMappingModel(model, null, false);
            //default values
            model.PageSize = _catalogSettings.DefaultManufacturerPageSize;
            model.PageSizeOptions = _catalogSettings.DefaultManufacturerPageSizeOptions;
            model.Published = true;
            model.AllowCustomersToSelectPageSize = true;

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(EmailGroupModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var emailGroup = model.ToEntity();
                emailGroup.CreatedOnUtc = DateTime.UtcNow;
                emailGroup.UpdatedOnUtc = DateTime.UtcNow;
                emailGroup.CustomerId = _workContext.CurrentCustomer.Id;
                _emailManagementService.InsertEmailGroup(emailGroup);
                //search engine name
                model.SeName = emailGroup.ValidateSeName(model.SeName, emailGroup.Name, true);
                _urlRecordService.SaveSlug(emailGroup, model.SeName, 0);
                //locales
                UpdateLocales(emailGroup, model);

                _emailManagementService.UpdateEmailGroup(emailGroup);
                
                //ACL (customer roles)
                SaveEmailGroupAcl(emailGroup, model);
                //Stores
                SaveStoreMappings(emailGroup, model);

                //activity log
                _customerActivityService.InsertActivity("AddNewEmailGroup", _localizationService.GetResource("ActivityLog.AddNewEmailGroup"), emailGroup.Name);

                SuccessNotification(_localizationService.GetResource("Admin.EmailManagement.EmailGroups.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = emailGroup.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            //templates
            PrepareTemplatesModel(model);
            //ACL
            PrepareAclModel(model, null, true);
            //Stores
            PrepareStoresMappingModel(model, null, true);

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            var emailGroup = _emailManagementService.GetEmailGroupById(id);
            if (emailGroup == null || emailGroup.Deleted)
                //No manufacturer found with the specified id
                return RedirectToAction("List");

            var model = emailGroup.ToModel();
            //locales
            AddLocales(_languageService, model.Locales, (locale, languageId) =>
            {
                locale.Name = emailGroup.GetLocalized(x => x.Name, languageId, false, false);
                locale.Description = emailGroup.GetLocalized(x => x.Description, languageId, false, false);
                locale.MetaKeywords = emailGroup.GetLocalized(x => x.MetaKeywords, languageId, false, false);
                locale.MetaDescription = emailGroup.GetLocalized(x => x.MetaDescription, languageId, false, false);
                locale.MetaTitle = emailGroup.GetLocalized(x => x.MetaTitle, languageId, false, false);
                locale.SeName = emailGroup.GetSeName(languageId, false, false);
            });

            //templates
            PrepareTemplatesModel(model);
            //ACL
            PrepareAclModel(model, emailGroup, false);
            //Stores
            PrepareStoresMappingModel(model, emailGroup, false);

            return View(model);
        }


        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(EmailGroupModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            var emailGroup = _emailManagementService.GetEmailGroupById(model.Id);
            if (emailGroup == null || emailGroup.Deleted)
                //No emailGroup found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                //int prevPictureId = emailGroup.PictureId;
                emailGroup = model.ToEntity(emailGroup);
                emailGroup.UpdatedOnUtc = DateTime.UtcNow;
                emailGroup.CustomerId = _workContext.CurrentCustomer.Id;
                _emailManagementService.UpdateEmailGroup(emailGroup);
                //search engine name
                model.SeName = emailGroup.ValidateSeName(model.SeName, emailGroup.Name, true);
                _urlRecordService.SaveSlug(emailGroup, model.SeName, 0);
                //locales
                UpdateLocales(emailGroup, model);

                _emailManagementService.UpdateEmailGroup(emailGroup);
              
                //ACL
                SaveEmailGroupAcl(emailGroup, model);
                //Stores
                SaveStoreMappings(emailGroup, model);

                //activity log
                _customerActivityService.InsertActivity("EditEmailGroup", _localizationService.GetResource("ActivityLog.EditEmailGroup"), emailGroup.Name);

                SuccessNotification(_localizationService.GetResource("Admin.EmailManagement.EmailGroups.Updated"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabIndex();

                    return RedirectToAction("Edit", new { id = emailGroup.Id });
                }
                return RedirectToAction("List");
            }


            //If we got this far, something failed, redisplay form
            //templates
            PrepareTemplatesModel(model);
            //ACL
            PrepareAclModel(model, emailGroup, true);
            //Stores
            PrepareStoresMappingModel(model, emailGroup, true);

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            var emailGroup = _emailManagementService.GetEmailGroupById(id);
            if (emailGroup == null)
                //No manufacturer found with the specified id
                return RedirectToAction("List");

            _emailManagementService.DeleteEmailGroup(emailGroup);

            //activity log
            _customerActivityService.InsertActivity("DeleteEmailGroup", _localizationService.GetResource("ActivityLog.DeleteEmailGroup"), emailGroup.Name);

            SuccessNotification(_localizationService.GetResource("Admin.EmailManagement.EmailGroups.Deleted"));
            return RedirectToAction("List");
        }


        #endregion

        #region Export / Import

        public ActionResult ExportXml()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            try
            {
                var emailGroups = _emailManagementService.GetAllEmailGroups(showHidden: true);
                var xml = _exportManager.ExportEmailGroupsToXml(emailGroups);
                return new XmlDownloadResult(xml, "emailgroups.xml");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

        #endregion

        #region EmailLists

        [HttpPost]
        public ActionResult EmailsList(DataSourceRequest command, int emailGroupId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            var emaillistEmailGroups = _emailManagementService.GetEmailListEmailGroupsByEmailGroupId(emailGroupId,
                command.Page - 1, command.PageSize, true);

            var gridModel = new DataSourceResult
            {
                Data = emaillistEmailGroups
                .Select(x => new EmailGroupModel.EmailGroupEmailListModel
                {
                    Id = x.Id,
                    EmailGroupId = x.EmailGroupId,
                    EmailListId = x.EmailListId,
                    Email = _emailListService.GetEmailListById(x.EmailListId).Email,
                    //DisplayOrder = x.DisplayOrder
                }),
                Total = emaillistEmailGroups.TotalCount
            };


            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult EmailListUpdate(EmailGroupModel.EmailGroupEmailListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            var emaillistEmailGroup = _emailManagementService.GetEmailListEmailGroupById(model.Id);
            if (emaillistEmailGroup == null)
                throw new ArgumentException("No emaillist emailgroups mapping found with the specified id");

            emaillistEmailGroup.DisplayOrder = model.DisplayOrder;

            _emailManagementService.UpdateEmailListEmailGroup(emaillistEmailGroup);

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult EmailListDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            var emaillistEmailGroup = _emailManagementService.GetEmailListEmailGroupById(id);
            if (emaillistEmailGroup == null)
                throw new ArgumentException("No emaillist emailgroup mapping found with the specified id");

            //var emailGroupId = emaillistEmailGroup.emailGroupId;
            _emailManagementService.DeleteEmailListEmailGroup(emaillistEmailGroup);

            return new NullJsonResult();
        }

        public ActionResult EmailListAddPopup(int emailGroupId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            var model = new EmailGroupModel.AddEmailGroupEmailListModel();

            //manufacturers
            model.AvailableEmailGroups.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var m in _emailManagementService.GetAllEmailGroups(showHidden: true))
                model.AvailableEmailGroups.Add(new SelectListItem { Text = m.Name, Value = m.Id.ToString() });

            //stores
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

            return View(model);
        }

        [HttpPost]
        public ActionResult EmailListAddPopupList(DataSourceRequest command)
        {
            //, EmailGroupModel.AddEmailGroupEmailListModel model
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

           // var gridModel = new DataSourceResult();
            //var emailLists = _emailListService.SearchEmailLists(
            //    emailGroupId: model.SearchEmailGroupId,
            //    storeId: model.SearchStoreId,
            //    pageIndex: command.Page - 1,
            //    pageSize: command.PageSize);

            //gridModel.Data = emailLists.Select(x => x.ToModel());
            //gridModel.Total = emailLists.TotalCount;

            //return Json(gridModel);
            //var emailLists = _emailListService.GetAllEmailLists();
            var emailLists = _emailListService.GetAllEmailListsByCustomerId(_workContext.CurrentCustomer.Id);
            var gridModel = new DataSourceResult
            {
                Data = emailLists.Select(x =>
                {
                    var model = x.ToModel();

                    return model;
                }),
                Total = emailLists.Count
            };
            return Json(gridModel);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public ActionResult EmailListAddPopup(string btnId, string formId, EmailGroupModel.AddEmailGroupEmailListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            if (model.SelectedEmailListIds != null)
            {
                foreach (int id in model.SelectedEmailListIds)
                {
                    var emailList = _emailListService.GetEmailListById(id);
                    if (emailList != null)
                    {
                        var existingEmailListEmailGroups = _emailManagementService.GetEmailListEmailGroupsByEmailGroupId(model.EmailGroupId, showHidden: true);
                        if (existingEmailListEmailGroups.FindEmailListEmailGroup(id, model.EmailGroupId) == null)
                        {
                            _emailManagementService.InsertEmailListEmailGroup(
                                new EmailGroupEmailList
                                {
                                    EmailGroupId = model.EmailGroupId,
                                    EmailListId = id,
                                    DisplayOrder = 1
                                });
                        }
                    }
                }
            }

            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            ViewBag.formId = formId;
            return View(model);
        }

        #endregion

        [HttpPost]
        public ActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailGroups))
                return AccessDeniedView();

            var emailgroups = new List<EmailGroup>();
            if (selectedIds != null)
            {
                emailgroups.AddRange(_emailManagementService.GetEmailGroupsByIds(selectedIds.ToArray()));

                for (int i = 0; i < emailgroups.Count; i++)
                {
                    var emailgroup = emailgroups[i];

                    //a vendor should have access only to his products
                    //if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                    //    continue;

                    _emailManagementService.DeleteEmailGroup(emailgroup);
                }
            }

            return Json(new { Result = true });
        }

    }
}