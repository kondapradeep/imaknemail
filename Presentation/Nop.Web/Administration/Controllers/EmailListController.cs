﻿using Nop.Admin.Models.EmailManagement;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.EmailManagement;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.EmailManagement;
using Nop.Services.ExportImport;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using System.Text;
using Nop.Core.Domain.Directory;
using Nop.Services.Directory;

namespace Nop.Admin.Controllers
{
    public partial class EmailListController : BaseAdminController
    {
        #region Fields

        private readonly IEmailManagementListService _emailListService;
        private readonly IEmailListTemplateService _emailListTemplateService;
        private readonly ICategoryService _categoryService;
        private readonly IEmailManagementService _emailGroupService;
        private readonly ICustomerService _customerService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IWorkContext _workContext;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IPdfService _pdfService;
        private readonly IExportManager _exportManager;
        private readonly IImportManager _importManager;
        private readonly IPermissionService _permissionService;
        private readonly IAclService _aclService;
        private readonly IStoreService _storeService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly ICacheManager _cacheManager;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICustomerActivityService _customerActivityService;
      //  private readonly EmailListSettings _emailListSettings;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
      

        #endregion

        #region Constructors

        public EmailListController(IEmailManagementListService emailListService,
            IEmailListTemplateService emailListTemplateService,
            ICategoryService categoryService, 
            IEmailManagementService emailGroupService,
            ICustomerService customerService,
            IUrlRecordService urlRecordService, 
            IWorkContext workContext, 
            ILanguageService languageService, 
            ILocalizationService localizationService, 
            ILocalizedEntityService localizedEntityService,
            ICustomerActivityService customerActivityService,
            IProductTagService productTagService,
            ICopyProductService copyProductService, 
            IPdfService pdfService,
            IExportManager exportManager, 
            IImportManager importManager,
            IPermissionService permissionService, 
            IAclService aclService,
            IStoreService storeService,
            IStoreMappingService storeMappingService,
            ICacheManager cacheManager,
            IDateTimeHelper dateTimeHelper,
           // EmailListSettings emailListSettings,
            ICountryService countryService,
            IStateProvinceService stateProvinceService)
        {
            this._emailListService = emailListService;
            this._emailListTemplateService = emailListTemplateService;
            this._categoryService = categoryService;
            this._emailGroupService = emailGroupService;
            this._customerService = customerService;
            this._urlRecordService = urlRecordService;
            this._workContext = workContext;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._localizedEntityService = localizedEntityService;
            this._pdfService = pdfService;
            this._exportManager = exportManager;
            this._importManager = importManager;
            this._customerActivityService = customerActivityService;
            this._permissionService = permissionService;
            this._aclService = aclService;
            this._storeService = storeService;
            this._storeMappingService = storeMappingService;
            this._cacheManager = cacheManager;
            this._dateTimeHelper = dateTimeHelper;
          //  this._emailListSettings = emailListSettings;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void UpdateLocales(EmailList emailList, EmailListModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(emailList,
                                                               x => x.Email,
                                                               localized.Email,
                                                               localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(emailList,
                                                               x => x.MetaKeywords,
                                                               localized.MetaKeywords,
                                                               localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(emailList,
                                                               x => x.MetaDescription,
                                                               localized.MetaDescription,
                                                               localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(emailList,
                                                               x => x.MetaTitle,
                                                               localized.MetaTitle,
                                                               localized.LanguageId);

                //search engine name
                var seName = emailList.ValidateSeName(localized.SeName, localized.Email, false);
                _urlRecordService.SaveSlug(emailList, seName, localized.LanguageId);
            }
        }

        [NonAction]
        protected virtual void PrepareAclModel(EmailListModel model, EmailList emailList, bool excludeProperties)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            model.AvailableCustomerRoles = _customerService
                .GetAllCustomerRoles(true)
                .Select(cr => cr.ToModel())
                .ToList();
            if (!excludeProperties)
            {
                if (emailList != null)
                {
                    model.SelectedCustomerRoleIds = _aclService.GetCustomerRoleIdsWithAccess(emailList);
                }
            }
        }

        [NonAction]
        protected virtual void SaveEmailListAcl(EmailList emailList, EmailListModel model)
        {
            var existingAclRecords = _aclService.GetAclRecords(emailList);
            var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
            foreach (var customerRole in allCustomerRoles)
            {
                if (model.SelectedCustomerRoleIds != null && model.SelectedCustomerRoleIds.Contains(customerRole.Id))
                {
                    //new role
                    if (existingAclRecords.Count(acl => acl.CustomerRoleId == customerRole.Id) == 0)
                        _aclService.InsertAclRecord(emailList, customerRole.Id);
                }
                else
                {
                    //remove role
                    var aclRecordToDelete = existingAclRecords.FirstOrDefault(acl => acl.CustomerRoleId == customerRole.Id);
                    if (aclRecordToDelete != null)
                        _aclService.DeleteAclRecord(aclRecordToDelete);
                }
            }
        }

        [NonAction]
        protected virtual void PrepareStoresMappingModel(EmailListModel model, EmailList emailList, bool excludeProperties)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            model.AvailableStores = _storeService
                .GetAllStores()
                .Select(s => s.ToModel())
                .ToList();
            if (!excludeProperties)
            {
                if (emailList != null)
                {
                    model.SelectedStoreIds = _storeMappingService.GetStoresIdsWithAccess(emailList);
                }
            }
        }

        [NonAction]
        protected virtual void SaveStoreMappings(EmailList emailList, EmailListModel model)
        {
            var existingStoreMappings = _storeMappingService.GetStoreMappings(emailList);
            var allStores = _storeService.GetAllStores();
            foreach (var store in allStores)
            {
                if (model.SelectedStoreIds != null && model.SelectedStoreIds.Contains(store.Id))
                {
                    //new store
                    if (existingStoreMappings.Count(sm => sm.StoreId == store.Id) == 0)
                        _storeMappingService.InsertStoreMapping(emailList, store.Id);
                }
                else
                {
                    //remove store
                    var storeMappingToDelete = existingStoreMappings.FirstOrDefault(sm => sm.StoreId == store.Id);
                    if (storeMappingToDelete != null)
                        _storeMappingService.DeleteStoreMapping(storeMappingToDelete);
                }
            }
        }


        [NonAction]
        protected virtual void PrepareEmailListModel(EmailListModel model, EmailList emailList,
            bool setPredefinedValues, bool excludeProperties)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            if (emailList != null)
            {
                model.CreatedOn = _dateTimeHelper.ConvertToUserTime(emailList.CreatedOnUtc, DateTimeKind.Utc);
                model.UpdatedOn = _dateTimeHelper.ConvertToUserTime(emailList.UpdatedOnUtc, DateTimeKind.Utc);
            }

            //little performance hack here
            //there's no need to load attributes, categories, manufacturers when creating a new product
            //anyway they're not used (you need to save a product before you map add them)
            if (emailList != null)
            {

               //emailgroups
                foreach (var emailGroups in _emailGroupService.GetAllEmailGroupsByCustomerId(_workContext.CurrentCustomer.Id))
                  //  _emailGroupService.GetAllEmailGroups(showHidden: true))
                {
                    model.AvailableEmailGroups.Add(new SelectListItem
                    {
                        Text = emailGroups.Name,
                        Value = emailGroups.Id.ToString()
                    });
                }

            }

            //templates
            var templates = _emailListTemplateService.GetAllEmailListTemplates();
            foreach (var template in templates)
            {
                model.AvailableEmailListTemplates.Add(new SelectListItem
                {
                    Text = template.Name,
                    Value = template.Id.ToString()
                });
            }

            //default values
            if (setPredefinedValues)
            {
                model.Published = true;
            }

          
        }

        #endregion

        #region Email list / create / edit / delete

        //list emaillists
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            var model = new EmailListModel();

            //EmailGroups

            model.AvailableEmailGroups.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var m in _emailGroupService.GetAllEmailGroups(showHidden: true))
                model.AvailableEmailGroups.Add(new SelectListItem { Text = m.Name, Value = m.Id.ToString() });

            return View(model);
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

           // var emailLists = _emailListService.GetAllEmailLists();
            
            var emailLists = _emailListService.GetAllEmailListsByCustomerId(_workContext.CurrentCustomer.Id);
           
            var gridModel = new DataSourceResult
            {
                Data = emailLists.Select(x =>
                {
                    var model = x.ToModel();

                    //var emaillistHtmlSb = new StringBuilder("<div>");
                    //if (_emailListSettings.StreetAddressEnabled && !String.IsNullOrEmpty(model.Address1))
                    //    emaillistHtmlSb.AppendFormat("{0}<br />", Server.HtmlEncode(model.Address1));
                    //if (_emailListSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(model.Address2))
                    //    emaillistHtmlSb.AppendFormat("{0}<br />", Server.HtmlEncode(model.Address2));
                    //if (_emailListSettings.CityEnabled && !String.IsNullOrEmpty(model.City))
                    //    emaillistHtmlSb.AppendFormat("{0},", Server.HtmlEncode(model.City));
                    //if (_emailListSettings.StateProvinceEnabled && !String.IsNullOrEmpty(model.StateProvinceName))
                    //    emaillistHtmlSb.AppendFormat("{0},", Server.HtmlEncode(model.StateProvinceName));
                    //if (_emailListSettings.ZipPostalCodeEnabled && !String.IsNullOrEmpty(model.ZipPostalCode))
                    //    emaillistHtmlSb.AppendFormat("{0}<br />", Server.HtmlEncode(model.ZipPostalCode));
                    //if (_emailListSettings.CountryEnabled && !String.IsNullOrEmpty(model.CountryName))
                    //    emaillistHtmlSb.AppendFormat("{0}", Server.HtmlEncode(model.CountryName));

                    //emaillistHtmlSb.Append("</div>");
                    //model.EmailListHtml = emaillistHtmlSb.ToString();
                    return model;
                }),
                Total = emailLists.Count
            };
            return Json(gridModel);
        }


        //create product
        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            var model = new EmailListModel();
            
            PrepareEmailListModel(model, null, true, true);
            AddLocales(_languageService, model.Locales);
            PrepareAclModel(model, null, false);
            PrepareStoresMappingModel(model, null, false);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(EmailListModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
               //product
                var emailList = model.ToEntity();
                emailList.EmailListTemplateId = 1;
                emailList.Published = true;
                emailList.CreatedOnUtc = DateTime.UtcNow;
                emailList.UpdatedOnUtc = DateTime.UtcNow;
                emailList.CustomerId = _workContext.CurrentCustomer.Id;
                _emailListService.InsertEmailList(emailList);
                //search engine name
                model.SeName = emailList.ValidateSeName(model.SeName, emailList.Email, true);
                _urlRecordService.SaveSlug(emailList, model.SeName, 0);
                //locales
                UpdateLocales(emailList, model);
                //ACL (customer roles)
                SaveEmailListAcl(emailList, model);
                //Stores
                SaveStoreMappings(emailList, model);

                _emailListService.UpdateEmailList(emailList);

                //activity log
                _customerActivityService.InsertActivity("AddNewEmailList", _localizationService.GetResource("ActivityLog.AddNewEmailList"), emailList.Email);

                SuccessNotification(_localizationService.GetResource("Admin.EmailManagement.EmailLists.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = emailList.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareEmailListModel(model, null, false, true);
            PrepareAclModel(model, null, true);
            PrepareStoresMappingModel(model, null, true);
            return View(model);
        }


        //edit product
        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            var emailList = _emailListService.GetEmailListById(id);
            if (emailList == null || emailList.Deleted)
                //No emailList found with the specified id
                return RedirectToAction("List");

            var model = emailList.ToModel();

            PrepareEmailListModel(model, emailList, false, false);
            AddLocales(_languageService, model.Locales, (locale, languageId) =>
            {
                locale.Email = emailList.GetLocalized(x => x.Email, languageId, false, false);
                locale.MetaKeywords = emailList.GetLocalized(x => x.MetaKeywords, languageId, false, false);
                locale.MetaDescription = emailList.GetLocalized(x => x.MetaDescription, languageId, false, false);
                locale.MetaTitle = emailList.GetLocalized(x => x.MetaTitle, languageId, false, false);
                locale.SeName = emailList.GetSeName(languageId, false, false);
            });

            PrepareAclModel(model, emailList, false);
            PrepareStoresMappingModel(model, emailList, false);
            return View(model);
        }


        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(EmailListModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            var emailList = _emailListService.GetEmailListById(model.Id);
            if (emailList == null || emailList.Deleted)
                //No product found with the specified id
                return RedirectToAction("List");

           if (ModelState.IsValid)
            {
                //product
                emailList = model.ToEntity(emailList);
                emailList.UpdatedOnUtc = DateTime.UtcNow;
                emailList.CustomerId = _workContext.CurrentCustomer.Id;
                _emailListService.UpdateEmailList(emailList);
                //search engine name
                model.SeName = emailList.ValidateSeName(model.SeName, emailList.Email, true);
                _urlRecordService.SaveSlug(emailList, model.SeName, 0);
                //locales
                UpdateLocales(emailList, model);
                 //ACL (customer roles)
                SaveEmailListAcl(emailList, model);
                //Stores
                SaveStoreMappings(emailList, model);

                _emailListService.UpdateEmailList(emailList);
               
                //activity log
                _customerActivityService.InsertActivity("EditEmailList", _localizationService.GetResource("ActivityLog.EditEmailList"), emailList.Email);

                SuccessNotification(_localizationService.GetResource("Admin.EmailManagement.EmailLists.Updated"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabIndex();

                    return RedirectToAction("Edit", new { id = emailList.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
           PrepareEmailListModel(model, emailList, false, true);
           PrepareAclModel(model, emailList, true);
           PrepareStoresMappingModel(model, emailList, true);
            return View(model);
        }

        //delete product
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            var emailList = _emailListService.GetEmailListById(id);
            if (emailList == null)
                //No emailList found with the specified id
                return RedirectToAction("List");

            _emailListService.DeleteEmailList(emailList);

            //activity log
            _customerActivityService.InsertActivity("DeleteEmailList", _localizationService.GetResource("ActivityLog.DeleteEmailList"), emailList.Email);

            SuccessNotification(_localizationService.GetResource("Admin.EmailManagement.EmailLists.Deleted"));
            return RedirectToAction("List");
        }


        #endregion
    
        #region Emaillist Emailgroups


        [HttpPost]
        public ActionResult EmailListEmailGroupList(DataSourceRequest command, int emailListId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            //a vendor should have access only to his products
            //if (_workContext.CurrentCustomer != null)
            //{
            //    var emaillist = _emailListService.GetAllEmailListsByCustomerId(emailListId);
            //    if (emaillist != null && emaillist != _workContext.CurrentCustomer.Id)
            //    {
            //        return Content("This is not your product");
            //    }
            //}

            var emailListEmailGroups = _emailGroupService.GetEmailListEmailGroupsByEmailListId(emailListId, true);
            var emailListEmailGroupsModel = emailListEmailGroups
                .Select(x => new EmailListModel.EmailGroupEmailListModel
                {
                    Id = x.Id,
                    EmailGroup = _emailGroupService.GetEmailGroupById(x.EmailGroupId).Name,
                    EmailListId = x.EmailListId,
                    EmailGroupId = x.EmailGroupId,
                    DisplayOrder = x.DisplayOrder
                })
                .ToList();

            var gridModel = new DataSourceResult
            {
                Data = emailListEmailGroupsModel,
                Total = emailListEmailGroupsModel.Count
            };

            return Json(gridModel);
        }


        [HttpPost]
        public ActionResult EmailListEmailGroupInsert(EmailListModel.EmailGroupEmailListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            var emailListId = model.EmailListId;
            var emailGroupId = model.EmailGroupId;

            //a vendor should have access only to his products
            //if (_workContext.CurrentVendor != null)
            //{
            //    var product = _productService.GetProductById(productId);
            //    if (product != null && product.VendorId != _workContext.CurrentVendor.Id)
            //    {
            //        return Content("This is not your product");
            //    }
            //}

            var existingEmailsistEmailGroups = _emailGroupService.GetEmailListEmailGroupsByEmailGroupId(emailGroupId, showHidden: true);
            if (existingEmailsistEmailGroups.FindEmailListEmailGroup(emailListId, emailGroupId) == null)
            {
                var emailsistEmailGroup = new EmailGroupEmailList
                {
                    EmailListId = emailListId,
                    EmailGroupId = emailGroupId,
                    DisplayOrder = model.DisplayOrder
                };
                //a vendor cannot edit "IsFeaturedProduct" property
                //if (_workContext.CurrentVendor == null)
                //{
                //    productManufacturer.IsFeaturedProduct = model.IsFeaturedProduct;
                //}
                _emailGroupService.InsertEmailListEmailGroup(emailsistEmailGroup);
            }

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult EmailListEmailGroupUpdate(EmailListModel.EmailGroupEmailListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            var emaillistEmailGroup = _emailGroupService.GetEmailListEmailGroupById(model.Id);
            if (emaillistEmailGroup == null)
                throw new ArgumentException("No emaillist emailgroup mapping found with the specified id");

            //a vendor should have access only to his products
            //if (_workContext.CurrentVendor != null)
            //{
            //    var product = _productService.GetProductById(productManufacturer.ProductId);
            //    if (product != null && product.VendorId != _workContext.CurrentVendor.Id)
            //    {
            //        return Content("This is not your product");
            //    }
            //}

            emaillistEmailGroup.EmailGroupId = model.EmailGroupId;
            emaillistEmailGroup.DisplayOrder = model.DisplayOrder;
            //a vendor cannot edit "IsFeaturedProduct" property
            //if (_workContext.CurrentVendor == null)
            //{
            //    productManufacturer.IsFeaturedProduct = model.IsFeaturedProduct;
            //}
            _emailGroupService.UpdateEmailListEmailGroup(emaillistEmailGroup);

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult EmailListEmailGroupDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            var emaillistEmailGroup = _emailGroupService.GetEmailListEmailGroupById(id);
            if (emaillistEmailGroup == null)
                throw new ArgumentException("No emaillist emailgroup mapping found with the specified id");

            var emailListId = emaillistEmailGroup.EmailListId;

            //a vendor should have access only to his products
            //if (_workContext.CurrentVendor != null)
            //{
            //    var product = _productService.GetProductById(productId);
            //    if (product != null && product.VendorId != _workContext.CurrentVendor.Id)
            //    {
            //        return Content("This is not your product");
            //    }
            //}

            _emailGroupService.DeleteEmailListEmailGroup(emaillistEmailGroup);

            return new NullJsonResult();
        }

  
    #endregion

        #region ImportEmailList

        [HttpPost]
        public ActionResult ImportExcel()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            ////a vendor cannot import products
            //if (_workContext.CurrentVendor != null)
            //    return AccessDeniedView();

            try
            {
                var file = Request.Files["importexcelfile"];
                if (file != null && file.ContentLength > 0)
                {
                    _importManager.ImportEmailListsFromXlsx(file.InputStream);
                }
                else
                {
                    ErrorNotification(_localizationService.GetResource("Admin.Common.UploadFile"));
                    return RedirectToAction("List");
                }
                SuccessNotification(_localizationService.GetResource("Admin.EmailManagement.EmailLists.Imported"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }

        }

        #endregion

        [HttpPost]
        public ActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageEmailLists))
                return AccessDeniedView();

            var emaillists = new List<EmailList>();
            if (selectedIds != null)
            {
                emaillists.AddRange(_emailListService.GetEmailListsByIds(selectedIds.ToArray()));

                for (int i = 0; i < emaillists.Count; i++)
                {
                    var emaillist = emaillists[i];

                    ////a vendor should have access only to his products
                    //if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                    //    continue;

                    _emailListService.DeleteEmailList(emaillist);
                }
            }

            return Json(new { Result = true });
        }
    }
}