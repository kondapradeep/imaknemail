﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Messages
{
    public class EmailAccountListModel : BaseNopModel
    {
        public EmailAccountListModel()
        {
            EmailAccounts = new List<EmailAccountModel>();
        }

        public IList<EmailAccountModel> EmailAccounts { get; set; }
    }
}