﻿using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Web.Validators.Messages;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Messages
{
    [Validator(typeof(EmailAccountValidator))]
    public partial class EmailAccountModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.Email")]
        [AllowHtml]
        public string Email { get; set; }

        [NopResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.DisplayName")]
        [AllowHtml]
        public string DisplayName { get; set; }

        [NopResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.Host")]
        [AllowHtml]
        public string Host { get; set; }

        [NopResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.Port")]
        public int Port { get; set; }

        [NopResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.Username")]
        [AllowHtml]
        public string Username { get; set; }

        [NopResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.Password")]
        [AllowHtml]
        public string Password { get; set; }

        [NopResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.EnableSsl")]
        public bool EnableSsl { get; set; }

        [NopResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.UseDefaultCredentials")]
        public bool UseDefaultCredentials { get; set; }

        [NopResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.IsDefaultEmailAccount")]
        public bool IsDefaultEmailAccount { get; set; }


        [NopResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.SendTestEmailTo")]
        [AllowHtml]
        public string SendTestEmailTo { get; set; }


        public bool EmailEnabled { get; set; }
        public bool EmailRequired { get; set; }

        public bool DisplayNameEnabled { get; set; }

        public bool DisplayNameRequired { get; set; }

        public bool HostEnabled { get; set; }

        public bool HostRequired { get; set; }

        public bool PortEnabled { get; set; }
        public bool PortRequired { get; set; }
        public bool UsernameEnabled { get; set; }
        public bool UsernameRequired { get; set; }
        public bool PasswordEnabled { get; set; }
        public bool PasswordRequired { get; set; }
        public bool EnableSslEnabled { get; set; }
        public bool EnableSslRequired { get; set; }
        public bool UseDefaultCredentialsEnabled { get; set; }
        public bool UseDefaultCredentialsRequired { get; set; }

     //   public bool IsDefaultEmailAccountEnabled { get; set; }

     //   public bool IsDefaultEmailAccountRequired { get; set; }


    }
}