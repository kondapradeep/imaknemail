﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Web.Validators.Messages;

namespace Nop.Web.Models.Messages
{
    
        [Validator(typeof(CampaignValidator))]
        public partial class CampaignModel : BaseNopEntityModel
        {
            public CampaignModel()
            {
                this.AvailableStores = new List<SelectListItem>();
            }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Name")]
            [AllowHtml]
            public string Name { get; set; }

            public bool NameEnabled { get; set; }
            public bool NameRequired { get; set; }

            public bool SubjectEnabled { get; set; }
            public bool SubjectRequired { get; set; }

            public bool BodyEnabled { get; set; }
            public bool BodyRequired { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Subject")]
            [AllowHtml]
            public string Subject { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Body")]
            [AllowHtml]
            public string Body { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Store")]
            public int StoreId { get; set; }
            public IList<SelectListItem> AvailableStores { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.CreatedOn")]
            public DateTime CreatedOn { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.AllowedTokens")]
            public string AllowedTokens { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.TestEmail")]
            [AllowHtml]
            public string TestEmail { get; set; }
        }
    }
