﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Messages
{
    public partial class CampaignListModel : BaseNopModel
    {
        public CampaignListModel()
        {
            Campaigns = new List<CampaignModel>();
        }

        public IList<CampaignModel> Campaigns { get; set; }
    }

    //public partial class CampaignModel : BaseNopEntityModel
    //{
    //    public string Name { get; set; }

    //    public string Subject { get; set; }
    //}
}