﻿using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Customer
{
    public class CustomerCampaignListModel : BaseNopModel
    {
        public CustomerCampaignListModel()
        {
            Campaigns = new List<CampaignModel>();
        }

        public IList<CampaignModel> Campaigns { get; set; }
    }
}