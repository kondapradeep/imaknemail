﻿using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Customer
{
    public class CustomerCampaignEditModel : BaseNopModel
    {
        public CustomerCampaignEditModel()
        {
            this.Campaign = new CampaignModel();
        }

        public CampaignModel Campaign { get; set; }
    }
}