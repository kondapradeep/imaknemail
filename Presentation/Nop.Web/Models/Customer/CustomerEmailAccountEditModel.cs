﻿using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Customer
{
    public class CustomerEmailAccountEditModel : BaseNopModel
    {
        public CustomerEmailAccountEditModel()
        {
            this.EmailAccount = new EmailAccountModel();
        }

        public EmailAccountModel EmailAccount { get; set; }
    }
}