﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Common
{
    public partial class CountrySelectorModel : BaseNopModel
    {
        public CountrySelectorModel()
        {
            AvailableCountries = new List<CountryModel>();
        }

        public IList<CountryModel> AvailableCountries { get; set; }

        public int CurrentCountryId { get; set; }
    }
}