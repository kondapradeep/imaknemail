﻿using Nop.Core.Domain.EmailManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.EmailManagement
{
    public partial class EmailGroupEmailListMap : NopEntityTypeConfiguration<EmailGroupEmailList>
    {
        public EmailGroupEmailListMap()
        {
            this.ToTable("EmailList_EmailGroup_Mapping");
            this.HasKey(pm => pm.Id);

            this.HasRequired(pm => pm.EmailGroup)
                .WithMany()
                .HasForeignKey(pm => pm.EmailGroupId);


            this.HasRequired(pm => pm.EmailList)
                .WithMany(p => p.EmailGroupEmailLists)
                .HasForeignKey(pm => pm.EmailListId);
        }
    }
}
