﻿using Nop.Core.Domain.EmailManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.EmailManagement
{
    public partial class EmailGroupTemplateMap : NopEntityTypeConfiguration<EmailGroupTemplate>
    {
        public EmailGroupTemplateMap()
        {
            this.ToTable("EmailGroupTemplate");
            this.HasKey(et => et.Id);
            this.Property(et => et.Name).IsRequired().HasMaxLength(400);
            this.Property(et => et.ViewPath).IsRequired().HasMaxLength(400);
        }
    }
}