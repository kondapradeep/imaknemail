﻿using Nop.Core.Domain.EmailManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.EmailManagement
{
    public partial class EmailGroupMap : NopEntityTypeConfiguration<EmailGroup>
    {
        public EmailGroupMap()
        {
            this.ToTable("EmailGroup");
            this.HasKey(eg => eg.Id);
            this.Property(eg => eg.Name).IsRequired().HasMaxLength(400);
            this.Property(eg => eg.MetaKeywords).HasMaxLength(400);
            this.Property(eg => eg.MetaTitle).HasMaxLength(400);
            this.Property(eg => eg.PageSizeOptions).HasMaxLength(200);
            this.Property(eg => eg.CustomerId);

        }
    }
}
