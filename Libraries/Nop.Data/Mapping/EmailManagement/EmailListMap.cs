﻿using Nop.Core.Domain.EmailManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.EmailManagement
{
    public partial class EmailListMap : NopEntityTypeConfiguration<EmailList>
    {
        public EmailListMap()
        {
            this.ToTable("EmailList");
            this.HasKey(p => p.Id);
            this.Property(p => p.FirstName);
            this.Property(p => p.LastName);
            this.Property(p => p.Email);
            this.Property(p => p.Address1);
            this.Property(p => p.Address2);
            this.Property(p => p.Country);
            this.Property(p => p.State);
            this.Property(p => p.City);
            this.Property(p => p.ZipPostalCode);
            this.Property(p => p.PhoneNumber);
            this.Property(p => p.MetaKeywords).HasMaxLength(400);
            this.Property(p => p.MetaTitle).HasMaxLength(400);
            this.Property(p => p.CustomerId);
        }
    }
}
