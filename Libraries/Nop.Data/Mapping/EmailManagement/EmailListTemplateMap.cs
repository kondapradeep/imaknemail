﻿using Nop.Core.Domain.EmailManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.EmailManagement
{
    public partial class EmailListTemplateMap : NopEntityTypeConfiguration<EmailListTemplate>
    {
        public EmailListTemplateMap()
        {
            this.ToTable("EmailListTemplate");
            this.HasKey(p => p.Id);
            this.Property(p => p.Name).IsRequired().HasMaxLength(400);
            this.Property(p => p.ViewPath).IsRequired().HasMaxLength(400);
        }
    }
}
