using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class CustomerMap : NopEntityTypeConfiguration<Customer>
    {
        public CustomerMap()
        {
            this.ToTable("Customer");
            this.HasKey(c => c.Id);
            this.Property(u => u.Username).HasMaxLength(1000);
            this.Property(u => u.Email).HasMaxLength(1000);
            this.Property(u => u.SystemName).HasMaxLength(400);

            this.Ignore(u => u.PasswordFormat);

            this.HasMany(c => c.CustomerRoles)
                .WithMany()
                .Map(m => m.ToTable("Customer_CustomerRole_Mapping"));

            this.HasMany(c => c.Addresses)
                .WithMany()
                .Map(m => m.ToTable("CustomerAddresses"));

            //customer campaigns mapping
            //this.HasMany(c => c.Campaigns)
            //    .WithMany()
            //    .Map(m => m.ToTable("CustomerCampaigns"));


            //customer email accounts mapping
            //this.HasMany(c => c.EmailAccounts)
            //    .WithMany()
            //    .Map(m => m.ToTable("CustomerEmailAccounts"));

            //emailList
            //this.HasMany(c => c.EmailLists)
            //    .WithMany()
            //    .Map(m => m.ToTable("CustomerEmailLists"));
                

            this.HasOptional(c => c.BillingAddress);
            this.HasOptional(c => c.ShippingAddress);
        }
    }
}