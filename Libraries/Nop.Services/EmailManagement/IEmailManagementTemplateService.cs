﻿using Nop.Core.Domain.EmailManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.EmailManagement
{
    public partial interface IEmailManagementTemplateService
    {
        /// <summary>
        /// Delete email group template
        /// </summary>
        /// <param name="emailGroupTemplate">Email group template</param>
        void DeleteEmailGroupTemplate(EmailGroupTemplate emailGroupTemplate);

        /// <summary>
        /// Gets all email group templates
        /// </summary>
        /// <returns>Email group templates</returns>
        IList<EmailGroupTemplate> GetAllEmailGroupTemplates();

        /// <summary>
        /// Gets a email group template
        /// </summary>
        /// <param name="emailGroupTemplateId">Email Group template identifier</param>
        /// <returns>EmailGroup template</returns>
        EmailGroupTemplate GetEmailGroupTemplateById(int emailGroupTemplateId);

        /// <summary>
        /// Inserts email group template
        /// </summary>
        /// <param name="emailGroupTemplate">Email Group template</param>
        void InsertEmailGroupTemplate(EmailGroupTemplate emailGroupTemplate);

        /// <summary>
        /// Updates the email group template
        /// </summary>
        /// <param name="emailGroupTemplate">EmailGroup template</param>
        void UpdateEmailGroupTemplate(EmailGroupTemplate emailGroupTemplate);
    }
}
