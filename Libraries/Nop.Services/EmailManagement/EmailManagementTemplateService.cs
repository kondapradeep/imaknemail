﻿using Nop.Core.Data;
using Nop.Core.Domain.EmailManagement;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.EmailManagement
{
    public partial class EmailManagementTemplateService : IEmailManagementTemplateService
    {

        #region Fields

        private readonly IRepository<EmailGroupTemplate> _emailGroupTemplateRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

         #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="emailGroupTemplateRepository">Email group template repository</param>
        /// <param name="eventPublisher">Event published</param>
        public EmailManagementTemplateService(IRepository<EmailGroupTemplate> emailGroupTemplateRepository,
            IEventPublisher eventPublisher)
        {
            this._emailGroupTemplateRepository = emailGroupTemplateRepository;
            this._eventPublisher = eventPublisher;
        }

        /// <summary>
        /// Delete email group template
        /// </summary>
        /// <param name="emailGroupTemplate">Emailgroup template</param>
        public virtual void DeleteEmailGroupTemplate(EmailGroupTemplate emailGroupTemplate)
        {
            if (emailGroupTemplate == null)
                throw new ArgumentNullException("emailGroupTemplate");

            _emailGroupTemplateRepository.Delete(emailGroupTemplate);

            //event notification
            _eventPublisher.EntityDeleted(emailGroupTemplate);
        }

        /// <summary>
        /// Gets all email group templates
        /// </summary>
        /// <returns>Email group templates</returns>
        public virtual IList<EmailGroupTemplate> GetAllEmailGroupTemplates()
        {
            var query = from pt in _emailGroupTemplateRepository.Table
                        orderby pt.DisplayOrder
                        select pt;

            var templates = query.ToList();
            return templates;
        }

        /// <summary>
        /// Gets a email group template
        /// </summary>
        /// <param name="emailGroupTemplateId">Email group template identifier</param>
        /// <returns>Email group template</returns>
        public virtual EmailGroupTemplate GetEmailGroupTemplateById(int emailGroupTemplateId)
        {
            if (emailGroupTemplateId == 0)
                return null;

            return _emailGroupTemplateRepository.GetById(emailGroupTemplateId);
        }

        /// <summary>
        /// Inserts email group template
        /// </summary>
        /// <param name="emailGroupTemplate">Email group template</param>
        public virtual void InsertEmailGroupTemplate(EmailGroupTemplate emailGroupTemplate)
        {
            if (emailGroupTemplate == null)
                throw new ArgumentNullException("emailGroupTemplate");

            _emailGroupTemplateRepository.Insert(emailGroupTemplate);

            //event notification
            _eventPublisher.EntityInserted(emailGroupTemplate);
        }

        /// <summary>
        /// Updates the email group template
        /// </summary>
        /// <param name="emailGroupTemplate">Email group template</param>
        public virtual void UpdateEmailGroupTemplate(EmailGroupTemplate emailGroupTemplate)
        {
            if (emailGroupTemplate == null)
                throw new ArgumentNullException("emailGroupTemplate");

            _emailGroupTemplateRepository.Update(emailGroupTemplate);

            //event notification
            _eventPublisher.EntityUpdated(emailGroupTemplate);
        }
        
        #endregion


    }
}
