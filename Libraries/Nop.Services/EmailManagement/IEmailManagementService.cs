﻿using Nop.Core;
using Nop.Core.Domain.EmailManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.EmailManagement
{
    public partial interface IEmailManagementService
    {

        IList<EmailGroup> GetAllEmailGroupsByCustomerId(int customerId);

        IList<EmailGroup> GetAllEmailGroups();

        IPagedList<EmailGroup> GetAllEmailGroups(string emailGroupName = "",
            int pageIndex = 0,
            int pageSize = int.MaxValue,
            bool showHidden = true);

        /// <summary>
        /// Gets email groups by identifier
        /// </summary>
        /// <param name="emailGroupIds">Email group identifiers</param>
        /// <returns>EmailGroups</returns>
        IList<EmailGroup> GetEmailGroupsByIds(int[] emailGroupIds);


        EmailGroup GetEmailGroupById(int emailGroupId);

        void InsertEmailGroup(EmailGroup emailGroup);

        void UpdateEmailGroup(EmailGroup emailgroup);

        void DeleteEmailGroup(EmailGroup emailGroup);

        /// <summary>
        /// Deletes a emaillist emailgroup mapping
        /// </summary>
        /// <param name="emaillistEmailGroup">Emaillist emailgroup mapping</param>
        void DeleteEmailListEmailGroup(EmailGroupEmailList emaillistEmailGroup);

        IList<EmailGroupEmailList> GetEmailListEmailGroupsByEmailListId(int emailListId, bool showHidden = false);


        IPagedList<EmailGroupEmailList> GetEmailListEmailGroupsByEmailGroupId(int emailGroupId,
        int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);


        /// <summary>
        /// Gets a emailist emailgroup mapping 
        /// </summary>
        /// <param name="emaillistEmailGroupId">Emaillist emailgroup mapping identifier</param>
        /// <returns>Emaillist emailgroup mapping</returns>
        EmailGroupEmailList GetEmailListEmailGroupById(int emaillistEmailGroupId);

        /// <summary>
        /// Inserts a emaillist emailgroup mapping
        /// </summary>
        /// <param name="emaillistEmailGroup">Emaillist emailgroup mapping</param>
        void InsertEmailListEmailGroup(EmailGroupEmailList emaillistEmailGroup);

        /// <summary>
        /// Updates the emaillist emailgroup mapping
        /// </summary>
        /// <param name="emaillistEmailGroup">Emalilist emailgroup mapping</param>
        void UpdateEmailListEmailGroup(EmailGroupEmailList emaillistEmailGroup);
    }
}
