﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.EmailManagement;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Services.Customers;

namespace Nop.Services.EmailManagement
{
    public partial class EmailManagementService : IEmailManagementService
    {
        #region Constants
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : emailgroup ID
        /// </remarks>
        private const string EMAILGROUPS_BY_ID_KEY = "Nop.emailgroup.id-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string EMAILGROUPS_PATTERN_KEY = "Nop.emailgroup.";

        private const string EMAILGROUPS_ALL_KEY = "Nop.emailgroups.all-{0}";

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// {1} : emailgroup ID
        /// {2} : page index
        /// {3} : page size
        /// {4} : current customer ID
        /// {5} : store ID
        /// </remarks>
        private const string EMAILLISTEMAILGROUPS_ALLBYEMAILGROUPID_KEY = "Nop.emaillistemailgroup.allbyemailgroupid-{0}-{1}-{2}-{3}-{4}-{5}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// {1} : emaillist ID
        /// {2} : current customer ID
        /// {3} : store ID
        /// </remarks>
        private const string EMAILLISTEMAILGROUPS_ALLBYEMAILLISTID_KEY = "Nop.emailistemailgroup.allbyemaillistid-{0}-{1}-{2}-{3}";
         /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string EMAILLISTEMAILGROUPS_PATTERN_KEY = "Nop.emaillistemailgroup.";



        #endregion

        #region Fields

        private readonly IRepository<EmailGroup> _emailGroupRepository;
        private readonly IRepository<EmailGroupEmailList> _emaillistEmailGroupRepository;
        private readonly IRepository<EmailList> _emailListRepository;
        private readonly IRepository<AclRecord> _aclRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        private readonly CatalogSettings _catalogSettings;

        #endregion

        #region Ctor

        public EmailManagementService(ICacheManager cacheManager,
            IRepository<EmailGroup> emailGroupRepository,
            IRepository<EmailGroupEmailList> emaillistEmailGroupRepository,
            IRepository<EmailList> emailListRepository,
            IRepository<AclRecord> aclRepository,
            IRepository<StoreMapping> storeMappingRepository,
            IWorkContext workContext,
            IStoreContext storeContext,
            IEventPublisher eventPublisher,
            CatalogSettings catelogSettings)
        {
            this._cacheManager = cacheManager;
            this._aclRepository = aclRepository;
            this._emailGroupRepository = emailGroupRepository;
            this._eventPublisher = eventPublisher;
            this._storeContext = storeContext;
            this._storeMappingRepository = storeMappingRepository;
            this._workContext = workContext;
            this._catalogSettings = catelogSettings;
            this._emaillistEmailGroupRepository = emaillistEmailGroupRepository;
            this._emailListRepository = emailListRepository;

        }

        #endregion

        #region Methods

        public virtual IList<EmailGroup> GetAllEmailGroupsByCustomerId(int customerId)
        {
            var query = from c in _emailGroupRepository.Table
                        orderby c.CreatedOnUtc
                        where c.Deleted == false && c.CustomerId == customerId
                        select c;
            var emailGroups = query.ToList();

            return emailGroups;
        }

        public virtual IList<EmailGroup> GetAllEmailGroups()
        {
            var query = from c in _emailGroupRepository.Table
                        orderby c.CreatedOnUtc
                        where c.Deleted == false
                        select c;
            var emailGroups = query.ToList();

            return emailGroups;
        }

        /// <summary>
        /// Get email groups by identifiers
        /// </summary>
        /// <param name="emailGroupIds">Email group identifiers</param>
        /// <returns>EmailGroups</returns>
        public virtual IList<EmailGroup> GetEmailGroupsByIds(int[] emailGroupIds)
        {
            if (emailGroupIds == null || emailGroupIds.Length == 0)
                return new List<EmailGroup>();

            var query = from p in _emailGroupRepository.Table
                        where emailGroupIds.Contains(p.Id)
                        select p;
            var emailGroups = query.ToList();
            //sort by passed identifiers
            var sortedEmailGroups = new List<EmailGroup>();
            foreach (int id in emailGroupIds)
            {
                var emailGroup = emailGroups.Find(x => x.Id == id);
                if (emailGroup != null)
                    sortedEmailGroups.Add(emailGroup);
            }
            return sortedEmailGroups;
        }


        public virtual IPagedList<EmailGroup> GetAllEmailGroups(string emailGroupName = "",
         int pageIndex = 0,
         int pageSize = int.MaxValue,
         bool showHidden = false)
        {
            var query = _emailGroupRepository.Table;
            if (!showHidden)
                query = query.Where(m => m.Published);
            if (!String.IsNullOrWhiteSpace(emailGroupName))
                query = query.Where(m => m.Name.Contains(emailGroupName));
            query = query.Where(m => !m.Deleted);
            query = query.OrderBy(m => m.DisplayOrder);

            if (!showHidden && (!_catalogSettings.IgnoreAcl || !_catalogSettings.IgnoreStoreLimitations))
            {
                if (!_catalogSettings.IgnoreAcl)
                {
                    //ACL (access control list)
                    var allowedCustomerRolesIds = _workContext.CurrentCustomer.GetCustomerRoleIds();
                    query = from m in query
                            join acl in _aclRepository.Table
                            on new { c1 = m.Id, c2 = "EmailGroup" } equals new { c1 = acl.EntityId, c2 = acl.EntityName } into m_acl
                            from acl in m_acl.DefaultIfEmpty()
                            where !m.SubjectToAcl || allowedCustomerRolesIds.Contains(acl.CustomerRoleId)
                            select m;
                }
                if (!_catalogSettings.IgnoreStoreLimitations)
                {
                    //Store mapping
                    var currentStoreId = _storeContext.CurrentStore.Id;
                    query = from m in query
                            join sm in _storeMappingRepository.Table
                            on new { c1 = m.Id, c2 = "EmailGroup" } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into m_sm
                            from sm in m_sm.DefaultIfEmpty()
                            where !m.LimitedToStores || currentStoreId == sm.StoreId
                            select m;
                }
                //only distinct emailgroups (group by ID)
                query = from m in query
                        group m by m.Id
                            into mGroup
                            orderby mGroup.Key
                            select mGroup.FirstOrDefault();
                query = query.OrderBy(m => m.DisplayOrder);
            }

            var emailgroups = new PagedList<EmailGroup>(query, pageIndex, pageSize);
            return emailgroups;
        
        
        }



        public virtual EmailGroup GetEmailGroupById(int emailGroupId)
        {
            if (emailGroupId == 0)
                return null;
            string key = string.Format(EMAILGROUPS_BY_ID_KEY, emailGroupId);
            return _cacheManager.Get(key, () => _emailGroupRepository.GetById(emailGroupId));
                
        }

        public virtual void InsertEmailGroup(EmailGroup emailGroup)
        {
            if (emailGroup == null)
                throw new ArgumentNullException("emailGroup");
            _emailGroupRepository.Insert(emailGroup);

            //cache
            _cacheManager.RemoveByPattern(EMAILGROUPS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(emailGroup);
         }

        public virtual void UpdateEmailGroup(EmailGroup emailGroup)
        {
            if (emailGroup == null)
                throw new ArgumentNullException("emailGroup");

            _emailGroupRepository.Update(emailGroup);

            //cache
            _cacheManager.RemoveByPattern(EMAILGROUPS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(emailGroup);
        }
        public virtual void DeleteEmailGroup(EmailGroup emailGroup)
        {
            if (emailGroup == null)
                throw new ArgumentNullException("emailGroup");
            
            emailGroup.Deleted = true;
            UpdateEmailGroup(emailGroup);
        }


        #endregion

        #region EmailGroups and EmailList

        /// <summary>
        /// Deletes a emaillist emailgroup mapping
        /// </summary>
        /// <param name="productManufacturer">Emaillist emailgroup mapping</param>
        public virtual void DeleteEmailListEmailGroup(EmailGroupEmailList emaillistEailGroup)
        {
            if (emaillistEailGroup == null)
                throw new ArgumentNullException("productManufacturer");

            _emaillistEmailGroupRepository.Delete(emaillistEailGroup);

            //cache
            _cacheManager.RemoveByPattern(EMAILGROUPS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(EMAILLISTEMAILGROUPS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(emaillistEailGroup);
        }

        /// <summary>
        /// Gets a emaillist emailgroup mapping 
        /// </summary>
        /// <param name="emaillistEmailGroupId">EmailList emailgroup mapping identifier</param>
        /// <returns>Emaillist emailgroup mapping</returns>
        public virtual EmailGroupEmailList GetEmailListEmailGroupById(int emaillistEmailGroupId)
        {
            if (emaillistEmailGroupId == 0)
                return null;

            return _emaillistEmailGroupRepository.GetById(emaillistEmailGroupId);
        }


        /// <summary>
        /// Inserts a emailist emailgroup mapping
        /// </summary>
        /// <param name="emaillistEmailGroup">Emaillist enmailgroup mapping</param>
        public virtual void InsertEmailListEmailGroup(EmailGroupEmailList emaillistEmailGroup)
        {
            if (emaillistEmailGroup == null)
                throw new ArgumentNullException("emaillistEmailGroup");

            _emaillistEmailGroupRepository.Insert(emaillistEmailGroup);

            //cache
            _cacheManager.RemoveByPattern(EMAILGROUPS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(EMAILLISTEMAILGROUPS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(emaillistEmailGroup);
        }

        /// <summary>
        /// Updates the emaillist emailgroup mapping
        /// </summary>
        /// <param name="emaillistEmailGroup">EmailList emailgroup mapping</param>
        public virtual void UpdateEmailListEmailGroup(EmailGroupEmailList emaillistEmailGroup)
        {
            if (emaillistEmailGroup == null)
                throw new ArgumentNullException("emaillistEmailGroup");

            _emaillistEmailGroupRepository.Update(emaillistEmailGroup);

            //cache
            _cacheManager.RemoveByPattern(EMAILGROUPS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(EMAILLISTEMAILGROUPS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(emaillistEmailGroup);
        }

        /// <summary>
        /// Gets a emaillist emailgroup mapping collection
        /// </summary>
        /// <param name="emaillistId">EmailList identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>EmailList emailgroup mapping collection</returns>
        public virtual IList<EmailGroupEmailList> GetEmailListEmailGroupsByEmailListId(int emailListId, bool showHidden = false)
        {
            if (emailListId == 0)
                return new List<EmailGroupEmailList>();

            string key = string.Format(EMAILLISTEMAILGROUPS_ALLBYEMAILLISTID_KEY, showHidden, emailListId, _workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id);
            return _cacheManager.Get(key, () =>
            {
                var query = from pm in _emaillistEmailGroupRepository.Table
                            join m in _emailGroupRepository.Table on pm.EmailGroupId equals m.Id
                            where pm.EmailListId == emailListId &&
                                !m.Deleted &&
                                (showHidden || m.Published)
                            orderby pm.DisplayOrder
                            select pm;


                if (!showHidden && (!_catalogSettings.IgnoreAcl || !_catalogSettings.IgnoreStoreLimitations))
                {
                    if (!_catalogSettings.IgnoreAcl)
                    {
                        //ACL (access control list)
                        var allowedCustomerRolesIds = _workContext.CurrentCustomer.GetCustomerRoleIds();
                        query = from pm in query
                                join m in _emailGroupRepository.Table on pm.EmailGroupId equals m.Id
                                join acl in _aclRepository.Table
                                on new { c1 = m.Id, c2 = "EmailGroup" } equals new { c1 = acl.EntityId, c2 = acl.EntityName } into m_acl
                                from acl in m_acl.DefaultIfEmpty()
                                where !m.SubjectToAcl || allowedCustomerRolesIds.Contains(acl.CustomerRoleId)
                                select pm;
                    }

                    if (!_catalogSettings.IgnoreStoreLimitations)
                    {
                        //Store mapping
                        var currentStoreId = _storeContext.CurrentStore.Id;
                        query = from pm in query
                                join m in _emailGroupRepository.Table on pm.EmailGroupId equals m.Id
                                join sm in _storeMappingRepository.Table
                                on new { c1 = m.Id, c2 = "EmailGroup" } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into m_sm
                                from sm in m_sm.DefaultIfEmpty()
                                where !m.LimitedToStores || currentStoreId == sm.StoreId
                                select pm;
                    }

                    //only distinct emailgroups (group by ID)
                    query = from pm in query
                            group pm by pm.Id
                                into mGroup
                                orderby mGroup.Key
                                select mGroup.FirstOrDefault();
                    query = query.OrderBy(pm => pm.DisplayOrder);
                }

                var emaillistEmailGroups = query.ToList();
                return emaillistEmailGroups;
            });
        }


        /// <summary>
        /// Gets emaillist emailgroup collection
        /// </summary>
        /// <param name="emailgroupId">EmailGroupidentifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>EmailList emailgroup collection</returns>
        public virtual IPagedList<EmailGroupEmailList> GetEmailListEmailGroupsByEmailGroupId(int emailGroupId,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            if (emailGroupId == 0)
                return new PagedList<EmailGroupEmailList>(new List<EmailGroupEmailList>(), pageIndex, pageSize);

            string key = string.Format(EMAILLISTEMAILGROUPS_ALLBYEMAILGROUPID_KEY, showHidden, emailGroupId, pageIndex, pageSize, _workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id);
            return _cacheManager.Get(key, () =>
            {
                var query = from pm in _emaillistEmailGroupRepository.Table
                            join p in _emailListRepository.Table on pm.EmailListId equals p.Id
                            where pm.EmailGroupId == emailGroupId &&
                                  !p.Deleted &&
                                  (showHidden || p.Published)
                            orderby pm.DisplayOrder
                            select pm;

                if (!showHidden && (!_catalogSettings.IgnoreAcl || !_catalogSettings.IgnoreStoreLimitations))
                {
                    if (!_catalogSettings.IgnoreAcl)
                    {
                        //ACL (access control list)
                        var allowedCustomerRolesIds = _workContext.CurrentCustomer.GetCustomerRoleIds();
                        query = from pm in query
                                join m in _emailGroupRepository.Table on pm.EmailGroupId equals m.Id
                                join acl in _aclRepository.Table
                                on new { c1 = m.Id, c2 = "EmailGroup" } equals new { c1 = acl.EntityId, c2 = acl.EntityName } into m_acl
                                from acl in m_acl.DefaultIfEmpty()
                                where !m.SubjectToAcl || allowedCustomerRolesIds.Contains(acl.CustomerRoleId)
                                select pm;
                    }
                    if (!_catalogSettings.IgnoreStoreLimitations)
                    {
                        //Store mapping
                        var currentStoreId = _storeContext.CurrentStore.Id;
                        query = from pm in query
                                join m in _emailGroupRepository.Table on pm.EmailGroupId equals m.Id
                                join sm in _storeMappingRepository.Table
                                on new { c1 = m.Id, c2 = "EmailGroup" } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into m_sm
                                from sm in m_sm.DefaultIfEmpty()
                                where !m.LimitedToStores || currentStoreId == sm.StoreId
                                select pm;
                    }

                    //only distinct emailgroups (group by ID)
                    query = from pm in query
                            group pm by pm.Id
                                into pmGroup
                                orderby pmGroup.Key
                                select pmGroup.FirstOrDefault();
                    query = query.OrderBy(pm => pm.DisplayOrder);
                }

                var emaillistEmailGroups = new PagedList<EmailGroupEmailList>(query, pageIndex, pageSize);
                return emaillistEmailGroups;
            });
        }



        #endregion
    }
}
