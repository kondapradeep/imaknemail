﻿using Nop.Core.Domain.EmailManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.EmailManagement
{
    public partial interface IEmailListTemplateService
    {
        /// <summary>
        /// Delete emailList template
        /// </summary>
        /// <param name="emailListTemplate">EmailList template</param>
        void DeleteEmailListTemplate(EmailListTemplate emailListTemplate);

        /// <summary>
        /// Gets all emailList templates
        /// </summary>
        /// <returns>EmailList templates</returns>
        IList<EmailListTemplate> GetAllEmailListTemplates();

        /// <summary>
        /// Gets a emailList template
        /// </summary>
        /// <param name="emailListTemplateId">EmailList template identifier</param>
        /// <returns>EmailList template</returns>
        EmailListTemplate GetEmailListTemplateById(int emailListTemplateId);

        /// <summary>
        /// Inserts emailList template
        /// </summary>
        /// <param name="emailListTemplate">EmailList template</param>
        void InsertEmailListTemplate(EmailListTemplate emailListTemplate);

        /// <summary>
        /// Updates the emailList template
        /// </summary>
        /// <param name="emailListTemplate">EmailList template</param>
        void UpdateEmailListTemplate(EmailListTemplate emailListTemplate);
   
    }
}
