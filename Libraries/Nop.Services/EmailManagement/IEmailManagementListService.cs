﻿using Nop.Core;
using Nop.Core.Domain.EmailManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.EmailManagement
{
    public partial interface IEmailManagementListService
    {
        #region Email Lists

        IList<EmailList> GetAllEmailListsByCustomerId(int customerId);

        EmailList GetEmailListByEmail(string email);

        IList<EmailList> GetAllEmailLists();

        /// <summary>
        /// Delete a EmailList
        /// </summary>
        /// <param name="emailList">EmailList</param>
        void DeleteEmailList(EmailList emailList);

        /// <summary>
        /// Gets Email List
        /// </summary>
        /// <param name="emailListId">Email list identifier</param>
        /// <returns>Email List</returns>
        EmailList GetEmailListById(int emailListId);

        /// <summary>
        /// Gets email lists by identifier
        /// </summary>
        /// <param name="emailListIds">EmailList identifiers</param>
        /// <returns>EmailLists</returns>
        IList<EmailList> GetEmailListsByIds(int[] emailListIds);

        /// <summary>
        /// Inserts a emailList
        /// </summary>
        /// <param name="emailList">emailList</param>
        void InsertEmailList(EmailList emailList);

        /// <summary>
        /// Updates the emailList
        /// </summary>
        /// <param name="emailList">emailList</param>
        void UpdateEmailList(EmailList emailList);


        IPagedList<EmailList> SearchEmailLists(
       int pageIndex = 0,
       int pageSize = int.MaxValue,
       int emailGroupId = 0,
       int storeId = 0);

        IPagedList<EmailList> SearchEmailLists(
            out IList<int> filterableSpecificationAttributeOptionIds,
            bool loadFilterableSpecificationAttributeOptionIds = false,
            int pageIndex = 0,
            int pageSize = int.MaxValue,
            int emailGroupId = 0,
            int storeId = 0);

      #endregion
    }
}
