﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.EmailManagement;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.EmailManagement
{
    public partial class EmailManagementListService : IEmailManagementListService
    {
        #region Constants
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : email list ID
        /// </remarks>
        private const string EMAILLISTS_BY_ID_KEY = "Nop.emaillist.id-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string EMAILLISTS_PATTERN_KEY = "Nop.emaillist.";
        #endregion

        #region Fields

        private readonly IRepository<EmailList> _emailListRepository;
        private readonly IRepository<LocalizedProperty> _localizedPropertyRepository;
        private readonly IRepository<AclRecord> _aclRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly ILanguageService _languageService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IDataProvider _dataProvider;
        private readonly IDbContext _dbContext;
        private readonly ICacheManager _cacheManager;
        private readonly IWorkContext _workContext;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CommonSettings _commonSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly IEventPublisher _eventPublisher;
        private readonly IAclService _aclService;
        private readonly IStoreMappingService _storeMappingService;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="emailListRepository">Email list repository</param>
        /// <param name="localizedPropertyRepository">Localized property repository</param>
        /// <param name="aclRepository">ACL record repository</param>
        /// <param name="storeMappingRepository">Store mapping repository</param>
        /// <param name="languageService">Language service</param>
        /// <param name="workflowMessageService">Workflow message service</param>
        /// <param name="dataProvider">Data provider</param>
        /// <param name="dbContext">Database Context</param>
        /// <param name="workContext">Work context</param>
        /// <param name="localizationSettings">Localization settings</param>
        /// <param name="commonSettings">Common settings</param>
        /// <param name="catalogSettings">Catalog settings</param>
        /// <param name="eventPublisher">Event published</param>
        /// <param name="aclService">ACL service</param>
        /// <param name="storeMappingService">Store mapping service</param>
        public EmailManagementListService(ICacheManager cacheManager,
            IRepository<EmailList> emailListRepository,
            IRepository<LocalizedProperty> localizedPropertyRepository,
            IRepository<AclRecord> aclRepository,
            IRepository<StoreMapping> storeMappingRepository,
            ILanguageService languageService,
            IWorkflowMessageService workflowMessageService,
            IDataProvider dataProvider, 
            IDbContext dbContext,
            IWorkContext workContext,
            LocalizationSettings localizationSettings, 
            CommonSettings commonSettings,
            CatalogSettings catalogSettings,
            IEventPublisher eventPublisher,
            IAclService aclService,
            IStoreMappingService storeMappingService)
        {
            this._cacheManager = cacheManager;
            this._emailListRepository = emailListRepository;
            this._localizedPropertyRepository = localizedPropertyRepository;
            this._aclRepository = aclRepository;
            this._storeMappingRepository = storeMappingRepository;
            this._languageService = languageService;
            this._workflowMessageService = workflowMessageService;
            this._dataProvider = dataProvider;
            this._dbContext = dbContext;
            this._workContext = workContext;
            this._localizationSettings = localizationSettings;
            this._commonSettings = commonSettings;
            this._catalogSettings = catalogSettings;
            this._eventPublisher = eventPublisher;
            this._aclService = aclService;
            this._storeMappingService = storeMappingService;
        }

        #endregion

        #region Methods

        public virtual IList<EmailList> GetAllEmailListsByCustomerId(int customerId)
        {
            var query = from c in _emailListRepository.Table
                        orderby c.CreatedOnUtc
                        where c.Deleted == false && c.CustomerId == customerId
                        select c;
            var emailLists = query.ToList();

            return emailLists;
        }
        public virtual EmailList GetEmailListByEmail(string email)
        {
            if (String.IsNullOrEmpty(email))
                return null;

            email = email.Trim();

            var query = from p in _emailListRepository.Table
                        orderby p.Id
                        where !p.Deleted &&
                        p.Email == email
                        select p;
            var emailList = query.FirstOrDefault();
            return emailList;
        }
        public virtual IList<EmailList> GetAllEmailLists()
        {
            var query = from c in _emailListRepository.Table
                        orderby c.CreatedOnUtc
                        where c.Deleted == false
                        select c;
            var emailLists = query.ToList();

            return emailLists;
        }

        /// <summary>
        /// Delete a email list
        /// </summary>
        /// <param name="emailList">EmailList</param>
        public virtual void DeleteEmailList(EmailList emailList)
        {
            if (emailList == null)
                throw new ArgumentNullException("emailList");

            emailList.Deleted = true;
            //delete email list
            UpdateEmailList(emailList);
        }


        /// <summary>
        /// Gets email list
        /// </summary>
        /// <param name="emailListId">Email list identifier</param>
        /// <returns>EmailList</returns>

        public virtual EmailList GetEmailListById(int emailListId)
        {
            if (emailListId == 0)
                return null;

            string key = string.Format(EMAILLISTS_BY_ID_KEY, emailListId);
            return _cacheManager.Get(key, () => _emailListRepository.GetById(emailListId));
        }

        /// <summary>
        /// Get emaillists by identifiers
        /// </summary>
        /// <param name="emailListIds">Email list identifiers</param>
        /// <returns>EmailLists</returns>
        public virtual IList<EmailList> GetEmailListsByIds(int[] emailListIds)
        {
            if (emailListIds == null || emailListIds.Length == 0)
                return new List<EmailList>();

            var query = from p in _emailListRepository.Table
                        where emailListIds.Contains(p.Id)
                        select p;
            var emailLists = query.ToList();
            //sort by passed identifiers
            var sortedEmailLists = new List<EmailList>();
            foreach (int id in emailListIds)
            {
                var emailList = emailLists.Find(x => x.Id == id);
                if (emailList != null)
                    sortedEmailLists.Add(emailList);
            }
            return sortedEmailLists;
        }

        /// <summary>
        /// Inserts a emaillist
        /// </summary>
        /// <param name="emailList">EmailList</param>
        public virtual void InsertEmailList(EmailList emailList)
        {
            if (emailList == null)
                throw new ArgumentNullException("emailList");

            //insert
            _emailListRepository.Insert(emailList);

            //clear cache
            _cacheManager.RemoveByPattern(EMAILLISTS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(emailList);
        }

        /// <summary>
        /// Updates the emaillist
        /// </summary>
        /// <param name="emailList ">EmailList</param>
        public virtual void UpdateEmailList(EmailList emailList)
        {
            if (emailList == null)
                throw new ArgumentNullException("emailList");

            //update
            _emailListRepository.Update(emailList);

            //cache
            _cacheManager.RemoveByPattern(EMAILLISTS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(emailList);
        }

        #endregion


       public virtual IPagedList<EmailList> SearchEmailLists(
       int pageIndex = 0,
       int pageSize = int.MaxValue,
       int emailGroupId = 0,
       int storeId = 0)
        {
            IList<int> filterableSpecificationAttributeOptionIds;
            return SearchEmailLists(out filterableSpecificationAttributeOptionIds, false,
                pageIndex, pageSize, emailGroupId,
                storeId);
        }

         public virtual IPagedList<EmailList> SearchEmailLists(
            out IList<int> filterableSpecificationAttributeOptionIds,
            bool loadFilterableSpecificationAttributeOptionIds = false,
            int pageIndex = 0,
            int pageSize = 2147483647,  //Int32.MaxValue
            int emailGroupId = 0,
            int storeId = 0)
        {
            filterableSpecificationAttributeOptionIds = new List<int>();
       
            if (_commonSettings.UseStoredProceduresIfSupported && _dataProvider.StoredProceduredSupported)
            {
                   if (pageSize == int.MaxValue)
                    pageSize = int.MaxValue - 1;
                

                 var pEmailGroupId = _dataProvider.GetParameter();
                pEmailGroupId.ParameterName = "EmailGroupId";
                pEmailGroupId.Value = emailGroupId;
                pEmailGroupId.DbType = DbType.Int32;

                var pStoreId = _dataProvider.GetParameter();
                pStoreId.ParameterName = "StoreId";
                pStoreId.Value = !_catalogSettings.IgnoreStoreLimitations ? storeId : 0;
                pStoreId.DbType = DbType.Int32;

                  var pPageIndex = _dataProvider.GetParameter();
                pPageIndex.ParameterName = "PageIndex";
                pPageIndex.Value = pageIndex;
                pPageIndex.DbType = DbType.Int32;

                var pPageSize = _dataProvider.GetParameter();
                pPageSize.ParameterName = "PageSize";
                pPageSize.Value = pageSize;
                pPageSize.DbType = DbType.Int32;

                
                var pTotalRecords = _dataProvider.GetParameter();
                pTotalRecords.ParameterName = "TotalRecords";
                pTotalRecords.Direction = ParameterDirection.Output;
                pTotalRecords.DbType = DbType.Int32;


                 //invoke stored procedure
                var emailLists = _dbContext.ExecuteStoredProcedureList<EmailList>(
                    "EmailListLoadAllPaged",
                    pEmailGroupId,
                    pStoreId,
                    pPageIndex,
                    pPageSize,
                    pTotalRecords);
        
                //return emaillists
                int totalRecords = (pTotalRecords.Value != DBNull.Value) ? Convert.ToInt32(pTotalRecords.Value) : 0;
                return new PagedList<EmailList>(emailLists, pageIndex, pageSize, totalRecords);

            }
            else
            {
                //stored procedures aren't supported. Use LINQ

                #region Search emaillists

                
                var query = _emailListRepository.Table;
                query = query.Where(p => !p.Deleted);


                if (storeId > 0 && !_catalogSettings.IgnoreStoreLimitations)
                {
                    //Store mapping
                    query = from p in query
                            join sm in _storeMappingRepository.Table
                            on new { c1 = p.Id, c2 = "EmailList" } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into p_sm
                            from sm in p_sm.DefaultIfEmpty()
                            where !p.LimitedToStores || storeId == sm.StoreId
                            select p;
                }


                //emailgroup filtering
                if (emailGroupId > 0)
                {
                    query = from p in query
                            from pm in p.EmailGroupEmailLists.Where(pm => pm.EmailGroupId == emailGroupId)
                            //where 
                            select p;
                }

                //only distinct emailgroup (group by ID)
                //if we use standard Distinct() method, then all fields will be compared (low performance)
                //it'll not work in SQL Server Compact when searching emaillists by a keyword)
                query = from p in query
                        group p by p.Id
                        into pGroup
                        orderby pGroup.Key
                        select pGroup.FirstOrDefault();

                var emailLists = new PagedList<EmailList>(query, pageIndex, pageSize);

                //return emaillists
                return emailLists;

                #endregion
            }
        }

    }
}
