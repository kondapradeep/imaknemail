﻿using Nop.Core.Domain.EmailManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.EmailManagement
{
    public static class EmailGroupExtensions
    {
        public static EmailGroupEmailList FindEmailListEmailGroup(this IList<EmailGroupEmailList> source,
          int emailListId, int emailGroupId)
        {
            foreach (var emaillistEmailGroup in source)
                if (emaillistEmailGroup.EmailListId == emailListId && emaillistEmailGroup.EmailGroupId == emailGroupId)
                    return emaillistEmailGroup;

            return null;
        }
    }
}
