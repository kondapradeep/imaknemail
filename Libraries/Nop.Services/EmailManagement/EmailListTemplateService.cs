﻿using Nop.Core.Data;
using Nop.Core.Domain.EmailManagement;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.EmailManagement
{
    public partial class EmailListTemplateService : IEmailListTemplateService
    {

       #region Fields

        private readonly IRepository<EmailListTemplate> _emailListTemplateRepository;
        private readonly IEventPublisher _eventPublisher;

      #endregion

       #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="emailListTemplateRepository">EmailList template repository</param>
        /// <param name="eventPublisher">Event published</param>
        public EmailListTemplateService(IRepository<EmailListTemplate> emailListTemplateRepository,
            IEventPublisher eventPublisher)
        {
            this._emailListTemplateRepository = emailListTemplateRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

       #region Methods

        /// <summary>
        /// Delete Email list template
        /// </summary>
        /// <param name="emailListTemplate">EmailList template</param>
        public virtual void DeleteEmailListTemplate(EmailListTemplate emailListTemplate)
        {
            if (emailListTemplate == null)
                throw new ArgumentNullException("emailListTemplate");

            _emailListTemplateRepository.Delete(emailListTemplate);

            //event notification
            _eventPublisher.EntityDeleted(emailListTemplate);
        }

        /// <summary>
        /// Gets all Email list templates
        /// </summary>
        /// <returns>EmailList templates</returns>
        public virtual IList<EmailListTemplate> GetAllEmailListTemplates()
        {
            var query = from pt in _emailListTemplateRepository.Table
                        orderby pt.DisplayOrder
                        select pt;

            var templates = query.ToList();
            return templates;
        }

        /// <summary>
        /// Gets a email list template
        /// </summary>
        /// <param name="emailListTemplateId">EmailList template identifier</param>
        /// <returns>EmailList template</returns>
        public virtual EmailListTemplate GetEmailListTemplateById(int emailListTemplateId)
        {
            if (emailListTemplateId == 0)
                return null;

            return _emailListTemplateRepository.GetById(emailListTemplateId);
        }

        /// <summary>
        /// Inserts email list template
        /// </summary>
        /// <param name="emailListTemplate">EmailList template</param>
        public virtual void InsertEmailListTemplate(EmailListTemplate emailListTemplate)
        {
            if (emailListTemplate == null)
                throw new ArgumentNullException("emailListTemplate");

            _emailListTemplateRepository.Insert(emailListTemplate);

            //event notification
            _eventPublisher.EntityInserted(emailListTemplate);
        }

        /// <summary>
        /// Updates the email list template
        /// </summary>
        /// <param name="emailListTemplate">EmailList template</param>
        public virtual void UpdateEmailListTemplate(EmailListTemplate emailListTemplate)
        {
            if (emailListTemplate == null)
                throw new ArgumentNullException("emailListTemplate");

            _emailListTemplateRepository.Update(emailListTemplate);

            //event notification
            _eventPublisher.EntityUpdated(emailListTemplate);
        }

        #endregion
    }
}
