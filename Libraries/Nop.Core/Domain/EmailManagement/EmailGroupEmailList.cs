﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.EmailManagement
{
    public partial class EmailGroupEmailList : BaseEntity
    {
        public int EmailListId { get; set; }

        /// <summary>
        /// Gets or sets the email group identifier
        /// </summary>
        public int EmailGroupId { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the emailGroup
        /// </summary>
        public virtual EmailGroup EmailGroup { get; set; }

        /// <summary>
        /// Gets or sets the emailList
        /// </summary>
        public virtual EmailList EmailList { get; set; }
    }
}
