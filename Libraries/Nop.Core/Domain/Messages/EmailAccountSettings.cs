﻿using Nop.Core.Configuration;

namespace Nop.Core.Domain.Messages
{
    public class EmailAccountSettings : ISettings
    {
        /// <summary>
        /// Gets or sets a store default email account identifier
        /// </summary>
        public int DefaultEmailAccountId { get; set; }

        public bool EmailEnabled { get; set; }
        public bool EmailRequired { get; set; }

        public bool DisplayNameEnabled { get; set; }

        public bool DisplayNameRequired { get; set; }

        public bool HostEnabled { get; set; }

        public bool HostRequired { get; set; }

        public bool PortEnabled { get; set; }
        public bool PortRequired { get; set; }

        public bool UsernameEnabled { get; set; }
        public bool UsernameRequired { get; set; }
        public bool PasswordEnabled { get; set; }
        public bool PasswordRequired { get; set; }

        public bool EnableSslEnabled { get; set; }
        public bool EnableSslRequired { get; set; }

        public bool UseDefaultCredentialsEnabled { get; set; }
        public bool UseDefaultCredentialsRequired { get; set; }

      //  public bool IsDefaultEmailAccountEnabled { get; set; }
      //  public bool IsDefaultEmailAccountRequired { get; set; }

    }

}
