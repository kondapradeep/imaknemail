﻿using Nop.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Messages
{
    public class CampaignSettings : ISettings
    {
        /// <summary>
        /// Gets or sets a value indicating whether 'Name' is enabled
        /// </summary>
        public bool NameEnabled { get; set; }

        public bool NameRequired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether 'Subject' is enabled
        /// </summary>
        public bool SubjectEnabled { get; set; }

        public bool SubjectRequired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether 'Subject' is enabled
        /// </summary>
        public bool BodyEnabled { get; set; }

        public bool BodyRequired { get; set; }

    }
}
